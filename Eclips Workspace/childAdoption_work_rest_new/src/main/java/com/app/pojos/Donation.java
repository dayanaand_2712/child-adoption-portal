package com.app.pojos;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name = "donation")
public class Donation
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "donarId")
	private int donarId;
	@Column(length = 50)
	private String donarName;
	@Column(name="Donation_purpose", length= 50)
	private String donationPurpose;
    @Column(name="Amount")
	private int amount;
    
    @JsonIgnore
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name="ngo_id")
  private Ngo ngo;

	public Ngo getNgo() {
	return ngo;
}





public void setNgo(Ngo ngo) {
	this.ngo = ngo;
}





	public Donation()
	{
		
	}
	
    

    
    
	public Donation(int donarId, String donarName, String donationPurpose, int amount) {
		super();
		this.donarId = donarId;
		this.donarName = donarName;
		this.donationPurpose = donationPurpose;
		this.amount = amount;
	}
	public int getDonarId() {
		return donarId;
	}
	public void setDonarId(int donarId) {
		this.donarId = donarId;
	}
	public String getDonarName() {
		return donarName;
	}
	public void setDonarName(String donarName) {
		this.donarName = donarName;
	}
	public String getDonationPurpose() {
		return donationPurpose;
	}
	public void setDonationPurpose(String donationPurpose) {
		this.donationPurpose = donationPurpose;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "Donation [donarId=" + donarId + ", donarName=" + donarName + ", donationPurpose=" + donationPurpose
				+ ", amount=" + amount + "]";
	}
}
