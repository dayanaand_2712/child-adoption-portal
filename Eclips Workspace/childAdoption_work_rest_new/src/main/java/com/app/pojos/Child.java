package com.app.pojos;


import java.time.LocalDate;

import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "children")

public class Child {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int child_id;
	private String child_name;
	private LocalDate birthDate;
	private int age;
	private String gender;
	private String color;
	
		//@JsonIgnore
	@Lob
	@Column(length=10000)
	private byte[]  imageUrl;
	
	@Column(name = "health_status")
	private String health;
	
	@Column(name = "blood_group")
	private String bloodGroup;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "reg_No",referencedColumnName = "reg_No")
	private Parent parent;
	
	//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@JsonIgnore
	@ManyToOne()
	@JoinColumn(name = "ngo_id",referencedColumnName = "ngo_id")
	private Ngo ngo;
	
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL ,mappedBy = "child", fetch = FetchType.LAZY,targetEntity = Request.class,orphanRemoval = true)
	private List<Request> requestList;
	
	
	public Child() {
		
	}
	


	public Child(/*int child_id,*/String child_name, LocalDate birthDate, int age, String gender, String color, String health,
			String bloodGroup,Ngo ngo) {
		super();
		//this.child_id = child_id;
		this.child_name = child_name;
		this.birthDate = birthDate;
		this.age = age;
		this.gender = gender;
		this.color = color;
		this.health = health;
		this.bloodGroup = bloodGroup;
		this.ngo=ngo;
	}


   public Child(int child_id, String child_name, LocalDate birthDate, int age, String gender, String color,/*byte[] imageUrl ,*/ String health,
			String bloodGroup, Parent parent, Ngo ngo, List<Request> requestList) {
		super();
		this.child_id = child_id;
		this.child_name = child_name;
		this.birthDate = birthDate;
		this.age = age;
		this.gender = gender;
		this.color = color;
		//this.imageUrl = imageUrl;
		this.health = health;
		this.bloodGroup = bloodGroup;
		this.parent = parent;
		this.ngo = ngo;
		this.requestList = requestList;
	} 
	
	
	
	public int getChild_id() {
		return child_id;
	}
	
	public void setChild_id(int child_id) {
		this.child_id = child_id;
	}

	public String getChild_name() {
		return child_name;
	}

	public void setChild_name(String child_name) {
		this.child_name = child_name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public byte[] getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(byte[] imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getHealth() {
		return health;
	}

	public void setHealth(String health) {
		this.health = health;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public Ngo getNgo() {
		return ngo;
	}

	public void setNgo(Ngo ngo) {
		this.ngo = ngo;
	}

	public List<Request> getRequestList() {
		return requestList;
	}

	public void setRequestList(List<Request> requestList) {
		this.requestList = requestList;
	}


	@Override
	public String toString() {
		return "Child [child_id=" + child_id + ", child_name=" + child_name + ", birthDate=" + birthDate + ", age="
				+ age + ", gender=" + gender + ", color=" + color + ", health=" + health + ", bloodGroup=" + bloodGroup
				+ ", parent=" + parent + ", ngo=" + ngo + ", requestList=" + requestList + "]";
	}
	
}
