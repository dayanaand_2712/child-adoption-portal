package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.Donation;


public interface IDonationRepository extends JpaRepository <Donation,Integer> {
	

}