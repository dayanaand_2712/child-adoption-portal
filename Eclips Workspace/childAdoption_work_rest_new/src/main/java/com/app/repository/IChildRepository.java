package com.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Child;




public interface IChildRepository extends JpaRepository<Child, Integer> {

	@Query("select c from Child c inner join Ngo n on c.ngo=n.ngo_id where n.ngo_id=:ngo")
	List<Child> listChildByNgoId(@Param("ngo") int ngoId);
}
