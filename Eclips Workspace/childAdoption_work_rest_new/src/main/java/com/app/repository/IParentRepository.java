package com.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.app.pojos.Parent;

public interface IParentRepository extends JpaRepository<Parent, Integer>{

	
	@Query("select u from Parent u where u.email=:email and u.password=:password")
	   Parent loginParent(@Param("email") String email, @Param("password") String password);
}
