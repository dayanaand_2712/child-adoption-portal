package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Child;
import com.app.pojos.Request;


public interface IRequestRepository extends JpaRepository<Request, Integer> {
	@Query("select r from Request r inner join Ngo n on r.ngo=n.ngo_id where n.ngo_id=:ngo")
	List<Request> listReqByNgoId(@Param("ngo") int ngoId);
	
/*	@Query("update Request r set r.statusOfRequest=:statusOfRequest where r.req_id=:req_id")
	Request updateRequest(@Param("req_id") int reqId,@Param("statusOdRequest") Request r);*/
	
}
