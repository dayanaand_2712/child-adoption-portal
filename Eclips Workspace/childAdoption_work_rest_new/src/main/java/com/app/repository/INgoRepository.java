package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.pojos.Ngo;



public interface INgoRepository extends JpaRepository<Ngo, Integer> {
	
	@Query("select u from Ngo u where u.email=:email and u.password=:password")
	   Ngo loginNgo(@Param("email") String email, @Param("password") String password);
	
	@Query("select u from Ngo u where u.email=:email")
    Ngo getByEmail(@Param("email") String email);
}
