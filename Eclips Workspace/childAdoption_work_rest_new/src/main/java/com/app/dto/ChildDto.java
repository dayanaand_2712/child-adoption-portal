package com.app.dto;

import com.app.pojos.Child;

public class ChildDto {

	private Child child;
	private int ngoId;
	//private int parentId;
	
	public ChildDto() {
		System.out.println("in cnstr of "+getClass().getName());
	}
	
	public ChildDto(Child child, int ngoId) {
		this.child = child;
		this.ngoId = ngoId;
	}
	public Child getChild() {
		return child;
	}
	public void setChild(Child child) {
		this.child = child;
	}
	public int getNgoId() {
		return ngoId;
	}
	public void setNgoId(int ngoId) {
		this.ngoId = ngoId;
	}
	@Override
	public String toString() {
		return "ChildDto [child=" + child + ", ngoId=" + ngoId + "]";
	}

   
}
