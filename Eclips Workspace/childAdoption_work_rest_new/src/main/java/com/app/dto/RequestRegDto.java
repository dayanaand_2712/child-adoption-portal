package com.app.dto;

import com.app.pojos.Request;

public class RequestRegDto {
	private Request request;
	private int ngoId;
	private int parentId;
	private int childId;

	RequestRegDto() {
		System.out.println("RCIp dto   ");
	}

	public RequestRegDto(Request request, int ngoId, int parentId, int childId) {
		super();
		this.request = request;
		this.ngoId = ngoId;
		this.parentId = parentId;
		this.childId = childId;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public int getNgoId() {
		return ngoId;
	}

	public void setNgoId(int ngoId) {
		this.ngoId = ngoId;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getChildId() {
		return childId;
	}

	public void setChildId(int childId) {
		this.childId = childId;
	}

	@Override
	public String toString() {
		return "RequestRegDto [request=" + request + ", ngoId=" + ngoId + ", parentId=" + parentId + ", childId="
				+ childId + "]";
	}
	
}
