package com.app.dto;

public class LoginDto {
	private  String email;
	private String password;
	
	public LoginDto() {
          System.out.println("in cnstr of "+getClass().getName());
 	}
	
	public LoginDto(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "LoginDTO [email=" + email + ", password=" + password + "]";
	}
	
}
