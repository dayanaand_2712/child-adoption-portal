package com.app.dto;

import com.app.pojos.Donation;


public class DonationDto
{
private Donation donation;
private int ngoId;

public DonationDto()
{

}

public DonationDto(Donation donation, int ngoId) {
	super();
	this.donation = donation;
	this.ngoId = ngoId;
}

public Donation getDonation() {
	return donation;
}

public void setDonation(Donation donation) {
	this.donation = donation;
}

public int getNgoId() {
	return ngoId;
}

public void setNgoId(int ngoId) {
	this.ngoId = ngoId;
}

@Override
public String toString() {
	return "DonationDto [donation=" + donation + ", ngoId=" + ngoId + "]";
}


}