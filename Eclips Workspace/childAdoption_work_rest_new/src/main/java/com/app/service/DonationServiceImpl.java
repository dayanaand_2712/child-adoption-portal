package com.app.service;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dto.DonationDto;
import com.app.pojos.Donation;

import com.app.repository.IDonationRepository;
import com.app.repository.INgoRepository;


@Service
@Transactional
public class DonationServiceImpl implements IDonationService {

	@Autowired
	private IDonationRepository donationDao;
	@Autowired
	private INgoRepository ngoDao;
	@Override
	public  Donation donates(@Valid DonationDto d ) 
	{
	    Donation donation = d.getDonation();
	    donation.setNgo(ngoDao.getOne(d.getNgoId()));
	    Donation donation2 = donationDao.save(donation);
		return  donation2;
	}
}
