package com.app.service;

import java.util.List;

import com.app.pojos.Child;
import com.app.pojos.Parent;

public interface IParentService {

	Parent registerParent(Parent p);
	Parent authenticateParent(String email,String password);
    Parent getparentDetails(Parent parent);
	
	boolean updateParent(int parent_id, Parent parent);
	
	boolean deleteParent(int parent_id);
	public List<Child> getChildListOnRequest(Child child); 
     
}
