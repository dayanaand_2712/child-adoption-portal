package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.app.dto.ChildDto;
import com.app.dto.RequestRegDto;
import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Request;
import com.app.repository.IChildRepository;
import com.app.repository.INgoRepository;


@Service
@Transactional
public class ChildServiceImpl implements IChildService {

	@Autowired
	private IChildRepository childDao;
	@Autowired
	private INgoRepository ngoDao;

	@Autowired
	private EntityManager mgr;
	
	/*@Override
	public Child registerChild(Child c) {
	
		return childDao.save(c);
	}*/
	
	
	
	@Override
	public Child regChild(@Valid ChildDto childDto) {
		Child child = childDto.getChild();
	    child.setNgo(ngoDao.getOne(childDto.getNgoId()));
		Child child2=childDao.save(child);
		return child2;
	}

	
	@Override
	public List<Child> listChild() {
		System.out.println("in method of "+getClass().getName());
		return childDao.findAll();
	}
	

	@Override
	public Optional<Child> getChildById(int id) {
		System.out.println("in method of "+getClass().getName());
		return childDao.findById(id);
	}
	
	@Override
	public boolean deleteChild(int id) {
		System.out.println("in service ngo delete meth");
		if(childDao.existsById(id))
		{
			childDao.deleteById(id);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean updateChild(int id, Child c) {
		System.out.println("in service ngo update meth");
		if(childDao.existsById(id))
		{
			System.out.println(c);
			mgr.unwrap(Session.class).update(c);
			return true;
		}
		return false;
	}

	/*@Override
	public Optional<Child> listChildByNgoId(@Valid ChildDto childdto) {
		Child child=childdto.getChild();
		child.setNgo(ngoDao.getOne(childdto.getNgoId()));
		return childDao.listChildByNgoId(child);
		
	}*/

	@Override
	public List<Child> listChildByNgoId(int ngoId) {
		System.out.println("in method of "+getClass().getName());
		return childDao.listChildByNgoId(ngoId);
	}


	
	
	
	


	

}
