package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.repository.INgoRepository;


@Service
@Transactional
public class NgoServiceImpl implements INgoService {

	@Autowired
	private INgoRepository ngoDao;
	
	@Autowired
	private EntityManager mgr;
	
	@Override
	public  Ngo registerNgo(Ngo p) {
	
		return  ngoDao.save(p);
	}
	
	@Override
	public List<Ngo> listNgo() {
		
		return ngoDao.findAll();
	}

	
	@Override
	public Ngo authenticateNgo(String email,String password ) {
		// TODO Auto-generated method stub
		System.out.println("in login service meth Email: "+email+" Password: "+password);
		return ngoDao.loginNgo(email, password);
	}

	@Override
	public boolean updateNgo(int ngoId, Ngo n) {
		System.out.println("in service ngo update meth");
		if(ngoDao.existsById(ngoId))
		{
			mgr.unwrap(Session.class).update(n);
			//ngoDao.update(n);
			return true;
		}
		return false;
	}

	
	
	@Override
	public boolean deleteNgo(int ngo_id) {
		System.out.println("in service ngo delete meth");
		if(ngoDao.existsById(ngo_id))
		{
			ngoDao.deleteById(ngo_id);
			return true;
		}
		return false;
	}

	@Override//original
	public List<Child> listChildren() {
		String jpql = "select c from Child c";
			//String jpql = "select c from Child c where c.ngo_id=:ngo_id";
			return mgr.unwrap(Session.class).createQuery(jpql,Child.class).getResultList();
		//return ngoDao.findAll();
		}

	@Override
	public List<Child> listChildren(int ngoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ngo getByEmail(String email) {
		System.out.println("in method of "+getClass().getName());
		return ngoDao.getByEmail(email);
	}
	
	//childList by ngoid
/*	@Override
	public Optional<Ngo> listChildrenByNgoiD(int ngoId) {
		
			String jpql = "select c from Child c where c.ngo_id=:ngo_id";
			return ngoDao.findById(ngoId);
         
		}*/
}
