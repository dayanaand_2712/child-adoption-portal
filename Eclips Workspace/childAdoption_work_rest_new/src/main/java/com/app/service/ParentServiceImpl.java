package com.app.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.pojos.Child;
import com.app.pojos.Parent;
import com.app.repository.IParentRepository;

@Service
@Transactional
public class ParentServiceImpl implements IParentService {

	@Autowired
	private IParentRepository parentDao;
	
	@Autowired
	private EntityManager mgr;
	
	@Override
	public Parent registerParent(Parent p) {
		
		return parentDao.save(p);
	}

	@Override
	public Parent authenticateParent(String email,String password) {
		System.out.println("in login  service method Email: "+email+" password: "+password);
		return parentDao.loginParent(email, password);
		
	}

	@Override
	public Parent getparentDetails(Parent parent) {
		String jpql = "Select u from Parent u where u.email=:pEmail";

		System.out.println(parent.getEmail());
		return mgr.unwrap(Session.class).createQuery(jpql,Parent.class).setParameter("pEmail",parent.getEmail()).getSingleResult();
	}

	@Override
	public boolean updateParent(int parent_id, Parent parent) {
		System.out.println("in method of "+getClass().getName());
		if(parentDao.existsById(parent_id)) {
			mgr.unwrap(Session.class).update(parent);
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteParent(int parent_id) {
		System.out.println("in method of "+getClass().getName());
		if(parentDao.existsById(parent_id)) {
			parentDao.deleteById(parent_id);
			return true;
		}
		return false;
	}

	@Override
	public List<Child> getChildListOnRequest(Child child) {
		String jpql;
		System.out.println("child "+child);
		
		System.out.println(child);
		jpql = "Select u from Child u where";

		if(child.getGender()!=null&&child.getAge()!=0)
		{
			jpql += " u.gender=:gen and u.age=:ag";
			return mgr.unwrap(Session.class).createQuery(jpql,Child.class)
					.setParameter("gen", child.getGender())
					.setParameter("ag", child.getAge())
                    .getResultList();
		}
		else if(child.getGender()!=null&& child.getAge()==0)
		{
			jpql += " u.gender=:gen";
			return mgr.unwrap(Session.class).createQuery(jpql,Child.class)
					.setParameter("gen", child.getGender())
					.setMaxResults(3)
					.getResultList();

		}
		else if( (child.getGender()==null&&child.getAge()!=0))
		{
			jpql += " u.age=:ag";
			return mgr.unwrap(Session.class).createQuery(jpql,Child.class)
					.setParameter("ag", child.getAge())
					.setMaxResults(3).getResultList();
		}
		return null;
	}

}
