package com.app.service;

import javax.validation.Valid;

import com.app.dto.DonationDto;
import com.app.pojos.Donation;


public interface IDonationService {

	//Donation donates(Donation d);
	Donation donates(@Valid DonationDto d);
	
}
