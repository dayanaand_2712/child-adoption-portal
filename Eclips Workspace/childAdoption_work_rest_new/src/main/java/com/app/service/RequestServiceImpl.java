package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.RequestRegDto;

import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Parent;
import com.app.pojos.Request;
import com.app.repository.IChildRepository;
import com.app.repository.INgoRepository;
import com.app.repository.IParentRepository;
import com.app.repository.IRequestRepository;

@Service
@Transactional
public class RequestServiceImpl implements IRequestService {

	@Autowired
	IRequestRepository dao;
	@Autowired
	private IChildRepository childDao;
	@Autowired
	private IParentRepository parentDao;
	@Autowired
	private INgoRepository ngoDao;

	@PersistenceContext
	EntityManager mgr;

	@Override
	public List<Request> listOfRequests() {
		System.out.println("in method of " + getClass().getName());
		return dao.findAll();
		
	}

	@Override
	public List<Request> listOfRequestsParent(Parent parent) {
		System.out.println("in method of " + getClass().getName());
		System.out.println(parent.getReg_No());
		String jpql = "SELECT r FROM Request r JOIN r.parent u WHERE u.reg_No=:parent";
		return mgr.unwrap(Session.class).createQuery(jpql, Request.class).setParameter("parent", parent.getReg_No())
				.getResultList();

	}

	@Override
	public List<Request> listOfRequestsNgo(Ngo ngo) {
		System.out.println("in method of " + getClass().getName());
		System.out.println(ngo.getNgo_id());
		String jpql = "SELECT r FROM Request r JOIN r.ngo u WHERE u.ngo_id=:ngo";
		return mgr.unwrap(Session.class).createQuery(jpql, Request.class).setParameter("ngo", ngo.getNgo_id())
				.getResultList();
	}

	//request from parent to ngo
	@Override
	public boolean regForCertainChild(@Valid RequestRegDto requestRegDto) {
		Request request = requestRegDto.getRequest();
		request.setChild(childDao.getOne(requestRegDto.getChildId()));
		request.setParent(parentDao.getOne(requestRegDto.getParentId()));
		request.setNgo(ngoDao.getOne(requestRegDto.getNgoId()));
		Request request2 = dao.save(request);
		if (request2 != null)
			return true;
		else
			return false;

	}

	/*@Override
	public Request detailsOfOneChild(int id) {
		System.out.println("in method of " + getClass().getName());
		String jpql = "select u from Request u where u.req_id=:reqid";
		Request reqresult = mgr.unwrap(Session.class).createQuery(jpql, Request.class).setParameter("reqid", id)
				.getSingleResult();
		return reqresult;
	}*/
	

	@Override
	public boolean responseforDetails(Request request) {
		System.out.println("in method of " + getClass().getName());
		request.setStatusOfRequest("responded");
		dao.save(request);
		return true;
	}

	@Override
	public Child oneRequestFromParent(int id) {
		// TODO Auto-generated method stub
		System.out.println("in method of " + getClass().getName());
		String jpql = "select u from Child u where u.child_id=:child_id";

		return mgr.unwrap(Session.class).createQuery(jpql, Child.class).setParameter("child_id", id).getSingleResult();

	}

	@Override
	public boolean deleteReq(int id) {
		System.out.println("in method of " + getClass().getName());
		if(dao.existsById(id)) {
			 dao.deleteById(id);
			 return true;
		}
		return false;
	}

	@Override
	public List<Request> listReqByNgoId(int ngoId) {
		System.out.println("in method of " + getClass().getName());
		return dao.listReqByNgoId(ngoId);
	}

	/*@Override
	public boolean updateRequest(int id, Request r) {
			System.out.println("in service req update meth");
			if(dao.existsById(id))
			{
				System.out.println(r);
				mgr.unwrap(Session.class).update(r);
				
								return true;
			}
			return false;
		}*/
	
	@Override
	public boolean updateRequest(int id,@Valid RequestRegDto r) {
			System.out.println("in service req update meth");
			Request request = r.getRequest();
			request.setChild(childDao.getOne(r.getChildId()));
			request.setParent(parentDao.getOne(r.getParentId()));
			request.setNgo(ngoDao.getOne(r.getNgoId()));
			Request req=dao.save(request);
			if(dao.existsById(id) && req!= null)
			{
				//System.out.println(r);
				return true;
			}
			return false;
		}

	
	@Override
	public Optional<Request> reqById(int reqId) {
		System.out.println("in service reqById meth");
		return dao.findById(reqId);
	}

	
}
