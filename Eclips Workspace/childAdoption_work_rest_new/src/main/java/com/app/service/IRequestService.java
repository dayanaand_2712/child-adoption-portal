package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.app.dto.RequestRegDto;

import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Parent;
import com.app.pojos.Request;


public interface IRequestService {

	List<Request> listOfRequests();
	
	public List<Request> listOfRequestsParent(Parent parent);
	
	public boolean regForCertainChild(@Valid RequestRegDto requestRegDto);
	
	//public Request detailsOfOneChild(int id);
	
	public boolean responseforDetails(Request request);
	
	public Child oneRequestFromParent(int id);

	List<Request> listOfRequestsNgo(Ngo ngo);

	boolean deleteReq(int id);
	List<Request> listReqByNgoId(int ngoId);
	Optional<Request> reqById(int reqId);
	//boolean updateRequest(int Id,Request r);
	boolean updateRequest(int id,@Valid RequestRegDto r);
	

}
