package com.app.service;

import java.util.List;

import com.app.pojos.Child;
import com.app.pojos.Ngo;



public interface INgoService {
	
	Ngo registerNgo(Ngo n);
	Ngo authenticateNgo(String email,String password);
	boolean updateNgo(int ngoId,Ngo n);
	public List<Ngo> listNgo();
	boolean deleteNgo(int ngo_id);
	public List<Child> listChildren();
	public List<Child> listChildren(int ngoId);
	Ngo getByEmail(String email);

}
