package com.app.service;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.repository.query.Param;

import java.util.List;

import com.app.dto.ChildDto;
import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Request;




public interface IChildService {

	
	//Child registerChild(Child c);
	Child regChild(@Valid ChildDto childDto);
	 public List<Child> listChild();
	Optional<Child> getChildById(int id);
	//Optional<Child> getChildById(int id);
	boolean deleteChild(int id);
	boolean updateChild(int Id,Child c);
	List<Child> listChildByNgoId(int ngoId);
	//Optional<Child> listChildByNgoId(@Valid ChildDto childdto);
	
	
}
