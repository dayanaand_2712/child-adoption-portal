package com.app.controlller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.app.dto.LoginDto;
import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.service.INgoService;

@RestController
@RequestMapping("/ngo")
@CrossOrigin(origins="http://localhost:3000")
//@CrossOrigin
public class NgoController {
	
	@Autowired
	private INgoService ngoService;
	
	public NgoController()
	{
		System.out.println("in cnstr of "+getClass().getName());
	}
	
	
	
	@PostMapping("/login")
	public Ngo  authNgo(@RequestBody LoginDto request)
	{
		System.out.println("ngo login"+request);
		Ngo ngo = ngoService.authenticateNgo(request.getEmail(),request.getPassword());
		System.out.println(request);
		return ngo;
	}
	
	
	//@PostMapping("/register")
	@PostMapping("/register")
	public ResponseEntity<?> registerNgo(@RequestBody Ngo n)
	{
		System.out.println("in reg ngo "+n);
		return ResponseEntity.ok(ngoService.registerNgo(n));
		
	/*	if(ngoService.registerNgo(n) != null)
		{
			return new ResponseEntity<Ngo>(n,HttpStatus.OK);
			
		}
		return new ResponseEntity<String>("Register failed:invalid credentials",HttpStatus.OK);	*/
	}
	
	@GetMapping("/getByEmail/{email}")
	public Ngo getByEmail(@PathVariable String email)
	{
		System.out.println("in meth of "+getClass().getName());
		System.out.println(email);
		return ngoService.getByEmail(email);
	}
	
	
	@PutMapping("/update/{ngo_id}")
	public ResponseEntity<?> updateNgo(@PathVariable int ngo_id,@RequestBody Ngo n)
	{
		
		if(ngoService.updateNgo(ngo_id,n)) 
		 {
		String msg = new String("Ngo account has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Ngo cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		
	}
	
	@GetMapping("/childlist")
	public List<Child> Showlist()
	{
		List<Child> list=ngoService.listChildren();
		return list;
	}
	
	@GetMapping("/ngolist")
	public List<Ngo> showListNgo()
	{
		List<Ngo> list=ngoService.listNgo();
		return list;
	}
	
	@DeleteMapping("/delete/{ngo_id}")
    public ResponseEntity<?> deleteNgo(@PathVariable int ngo_id)
	{
		
		if(ngoService.deleteNgo(ngo_id))
		{
		String msg = new String("Ngo account has been removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Ngo account cant be removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
}
