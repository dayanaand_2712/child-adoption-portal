package com.app.controlller;



import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ChildDto;
import com.app.dto.RequestRegDto;
import com.app.dto.ResponseDTO;
import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Request;
import com.app.service.IChildService;

@RestController
@RequestMapping("/child")
@CrossOrigin(origins="http://localhost:3000")
//@CrossOrigin
public class ChildController {

	
	@Autowired
	private IChildService childService;
	
	public ChildController()
	{
		System.out.println("in cnstr of "+getClass().getName());
	}
	
	
	/*@PostMapping("/register")
	public ResponseEntity<?> registerChild(@RequestBody Child c)
	{
		try{
			System.out.println("in reg child "+c);
			return ResponseEntity.ok(childService.registerChild(c));
			}catch(Exception e) { 
				e.printStackTrace();
				return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
			}
	}
	*/
	
	
	@PostMapping("/register")
	public ResponseEntity<?> registerChild(@Valid @RequestBody ChildDto childDto)
	{
		
		  try{ 
		  System.out.println("in reg child "+childDto);
		  return ResponseEntity.ok(childService.regChild(childDto)); 
		  }catch(Exception e) {
		  e.printStackTrace();
		  return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		  }
		 
		/*
		 * System.out.println("in reg child "+childDto);
		 * if(childService.regChild(childDto)) { return new
		 * ResponseEntity<String>("child info added successfully",HttpStatus.OK);
		 * 
		 * } return new
		 * ResponseEntity<String>("Data Fetch failed",HttpStatus.NOT_FOUND);
		 */
	}
	
	
	  @GetMapping("/list") 
	  public List<Child> listAllChild(){
	  
	  List<Child> child=childService.listChild(); 
	  return child;
	  
	  }
	 
	
	/*@GetMapping("/list")
	public ResponseDTO<?> getAllChildrens() {
		System.out.println("in get al users");
		return new ResponseDTO<>(HttpStatus.OK, "Fetching childrens list successfully", childService.listChild());
	}*/
	
    @GetMapping("/display/{id}")//child_id
	public Optional<Child> getChildById(@PathVariable int id )
	{
		System.out.println(id);
		Optional<Child> child=childService.getChildById(id);
		return child;
	}
	
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateChild(@PathVariable int id,@RequestBody Child c)
	{
		System.out.println(c);
		if(childService.updateChild(id,c)) 
		 {
		String msg = new String("Child information has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Child information cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		
	}
	
	@DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteChild(@PathVariable int id)
	{
		
		if(childService.deleteChild(id))
		{
		String msg = new String("child has been removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Child cant be removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
	@GetMapping("/childbyngoid/{ngo_id}")
	public List<Child> listChildByNgoId(@PathVariable int ngo_id) {
		System.out.println("in method of "+getClass().getName());
		System.out.println("ngo ID "+ngo_id);
		List<Child> child=childService.listChildByNgoId(ngo_id);
		return child;
	}

	/*@GetMapping("/childbyngoid/{ngoId}")
	public List<Child> listChildByNgoId(@RequestBody ChildDto request,@PathVariable int ngoId) {
		System.out.println("in method of "+getClass().getName());
		System.out.println("ngo ID "+ngoId);
		List<Child> child=childService.listChildByNgoId(ngoId);
		return child;
	}*/
	
	

}
