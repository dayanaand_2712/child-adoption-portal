package com.app.controlller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.RequestRegDto;

import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Parent;
import com.app.pojos.Request;
import com.app.service.IRequestService;


@RestController
@RequestMapping("/request")
@CrossOrigin(origins="http://localhost:3000")
//@CrossOrigin
public class RequestController {

	public RequestController() {
		System.out.println("in cnst of "+getClass().getName());
	}
	
	
	@Autowired
	IRequestService service;
	
	@GetMapping("/list")
	public List<Request> requests()
	{
		return service.listOfRequests();
	}
	
	//req parent to ngo
/*	@PostMapping("/certainchildtongo")
	public ResponseEntity<?> regForCertainChild(@Valid @RequestBody Request requestparams)
	{
		System.out.print(requestparams.getNgo());
		System.out.println(requestparams);
	

		if(service.regForCertainChild(requestparams))
		{
			return new ResponseEntity<String>("child request sent successfully",HttpStatus.OK);
			
		}
		return new ResponseEntity<String>("Data Fetch failed",HttpStatus.NOT_FOUND);
	}*/
	
	@PostMapping("/certainchildtongo")
	public ResponseEntity<?> regForCertainChild(@Valid @RequestBody RequestRegDto requestRegDto)
	{
		System.out.print(requestRegDto);
		//System.out.println(requestparams);
	

		if(service.regForCertainChild(requestRegDto))
		{
			return new ResponseEntity<String>("child request sent successfully",HttpStatus.OK);
			
		}
		return new ResponseEntity<String>("Data Fetch failed",HttpStatus.NOT_FOUND);
	}	
	
	
	
	
	@PostMapping("/ngorequestlist")//pass ngo_id
	public ResponseEntity<?> ngoReqList(@Valid @RequestBody Ngo ngo)
	{
		System.out.println(ngo.getNgo_id());
		List<Request>list=  service.listOfRequestsNgo(ngo);
		return new ResponseEntity<List<Request>>(list,HttpStatus.OK);
	}
	
	@GetMapping("/reqbyngoid/{ngo_id}")
	public List<Request> reqByNgoId(@PathVariable int ngo_id) {
		System.out.println("in method of "+getClass().getName());
		System.out.println("ngo ID "+ngo_id);
		List<Request> request=service.listReqByNgoId(ngo_id);
		return request;
	}
	
	@PostMapping("/parentrequestlist")//pass reg_no
	public ResponseEntity<?>  parentReqList(@Valid @RequestBody Parent parent)
	{
		System.out.println(parent.getReg_No());
		List<Request>list=  service.listOfRequestsParent(parent);
		return new ResponseEntity<List<Request>>(list,HttpStatus.OK);
	}
	
	
	
	
/*	@GetMapping("/viewlist/{id}")
	public ResponseEntity<?>  detailsOfOneChild(@PathVariable int id )
	{
		System.out.println(id);
		Request request = service.detailsOfOneChild(id);
		System.out.println("child:"+request.getChild());
		System.out.println("parent:"+request.getParent());
		System.out.println("\n\nRequest details:"+new ResponseEntity<Request>(request,HttpStatus.OK));
		return new ResponseEntity<Request>(request,HttpStatus.OK);
	}*/
	
	@GetMapping("/viewlist/{id}")
	public Optional<Request> getReqById(@PathVariable int id )
	{
		System.out.println(id);
		Optional<Request> req=service.reqById(id);
		return req;
	}
	
	
	@GetMapping("/oneRequestFromParent/{id}")//child_id
	public ResponseEntity<?> oneReqFromParent(@PathVariable int id )
	{
		System.out.println(id);
		Child request = service.oneRequestFromParent(id);		
		return new ResponseEntity<Child>(request,HttpStatus.OK);
	}

	@PutMapping("/responsefromngo")//req_id
	public ResponseEntity<?> resFromNgo(@Valid @RequestBody Request request)
	{
		service.responseforDetails(request);
		return new ResponseEntity<String>("updated",HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{req_id}")
    public ResponseEntity<?> deleteReq(@PathVariable int req_id)
	{
		
		if(service.deleteReq(req_id))
		{
		String msg = new String("Parent request has been removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Parent request cant be removed");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<?> updateRequest(@PathVariable int id,@Valid @RequestBody RequestRegDto r)
	{
		System.out.println("in put"+id);
		if(service.updateRequest(id,r)) 
		 {
		String msg = new String("Request has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Req cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		
	}
	
	/*
	 * @PutMapping("/update/{id}")
	public ResponseEntity<?> updateRequest(@PathVariable int id,@RequestBody Request r)
	{
		System.out.println("in put"+id);
		if(service.updateRequest(id,r)) 
		 {
		String msg = new String("Request has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Req cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		
	}
	
	 */

	

	
	
}
