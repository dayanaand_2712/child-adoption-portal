package com.app.controlller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.DonationDto;
import com.app.pojos.Donation;
import com.app.pojos.Parent;
import com.app.service.IDonationService;


@RestController
@RequestMapping("/donation")
@CrossOrigin(origins="http://localhost:3000")
//@CrossOrigin
public class DonationController 
{

	@Autowired
	private IDonationService donationService;
	
	public DonationController()
	{
		System.out.println("in cnstr of "+getClass().getName());
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> donation(@Valid @RequestBody DonationDto d)
	{
		try{
		System.out.println("in reg donation "+d);
		return ResponseEntity.ok(donationService.donates(d));
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
}
