package com.app.controlller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.LoginDto;
import com.app.pojos.Child;
import com.app.pojos.Ngo;
import com.app.pojos.Parent;
import com.app.service.IParentService;

@RestController
@RequestMapping("/parent")
@CrossOrigin(origins="http://localhost:3000")
//@CrossOrigin
public class ParentController {
	
	@Autowired
	private IParentService parentService;
	
	public ParentController()
	{
		System.out.println("in cnstr of "+getClass().getName());
	}
	
	
	@PostMapping("/login")
	public Parent authParent(@RequestBody LoginDto request)
	{
			System.out.println("ngo login"+request);
			Parent parent = parentService.authenticateParent(request.getEmail(),request.getPassword());
			System.out.println(request);
			return parent;
	}
	
	
	@PostMapping("/register")
	public ResponseEntity<?> registerParent(@RequestBody Parent p)
	{
		try{
		System.out.println("in reg ngo "+p);
		return ResponseEntity.ok(parentService.registerParent(p));
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
	}
	

	@PutMapping("/update/{parent_id}")
	public ResponseEntity<?> updateParentById(@PathVariable int parent_id,@RequestBody Parent p) {
		if(parentService.updateParent(parent_id,p)) 
		 {
		String msg = new String("Parent account has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Parent cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{parent_id}")
	public ResponseEntity<?> deleteParentById(@PathVariable int parent_id) {
		if(parentService.deleteParent(parent_id)) 
		 {
		String msg = new String("Parent account has been updated sucessfully.......");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
		}
		String msg = new String("Parent cant be updated.");
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
	
	@PostMapping("/reqchildonCondition")
	public ResponseEntity<?> m1(@RequestBody Child child)
	{		
		System.out.println(child);
		List<Child> tempList = parentService.getChildListOnRequest(child);
		System.out.println("temp "+tempList);

		if(tempList!=null)
		{
			return new ResponseEntity<List<Child>>(tempList,HttpStatus.OK);
			
		}
		return new ResponseEntity<String>("Data Fetch failed",HttpStatus.NOT_FOUND);
		
	}

	

}
