import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import './App.css';
import Navbar from "./component/navbar/Navbar.js"
import LoginNgoComponent from "./component/ngo/LoginNgoComponent.js";
import LoginParentComponent from "./component/parent/LoginParentComponent.jsx";
import RegisterNgoComponent from "./component/ngo/RegisterNgoComponent.jsx";
import RegisterParentComponent from "./component/parent/RegisterParentComponent.jsx";
import NgoHomePage from "./component/ngo/ngoHomePage.js";
import ParentHomePage from "./component/parent/parentHomePage.js";
//import background from './images/250Family.jpg';
import image from './images/Kids.png';
import image1 from './images/happy_children.jpg';
//import image2 from './images/mom.jpg';
import image3 from './images/parent.jpg';
//import logo1 from './images/logo.png';
//import image4 from './images/homepic.jpg'
//import galleryCompenent from "./component/Gallery/galleryComponent.js";
import "react-image-gallery/styles/css/image-gallery.css";
import gallery from './component/Gallery/gindex.jsx';
import faq from './component/FAQ/fqa.jsx';
import aboutus from './component/About/aboutus.jsx';
import contact from './component/Contact/contact.jsx';
import Home from './component/Home/homePage.jsx';
import Donation from './component/Donation/Donation.js';

class App extends Component { 
    render(){
      return (
      <div >
      <BasicRouting></BasicRouting>
      </div>
      );
    }
}

export default App;

class BasicRouting extends Component{
  render(){
    return(
      <div >
       
      <Router>
        {/*
              <headerbutton  className="App-headerbutton" >
          <Link to="/" >Home</Link> &emsp;
          <Link to="/login">Login</Link> &emsp;
          <Link to="/register">Register</Link>
      </headerbutton>*/}
      
        <Navbar/>
        <Switch>
          <Route  path="/" exact component={Home} />
          <Route path="/ngo-home" component={NgoHomePage}/>
          <Route path="/parent-home" component={ParentHomePage}/>
          <Route path="/login" component={Login} />
          <Route path="/ngo-login" component={LoginNgoComponent} />
          <Route path="/parent-login" component={LoginParentComponent} />
          <Route path="/register" component={Register} />
          <Route path="/ngo-register" component={RegisterNgoComponent} />
          <Route path="/parent-register" component={RegisterParentComponent} />
          <Route path="/gallery" component={gallery} />
          <Route path="/faq" component={faq} />
          <Route path="/aboutus" component={aboutus} />
          <Route path="/contact-us" component={contact} />
          <Route path="/donation" component={Donation} />
        </Switch>
      </Router>
      </div>
    );
  } 
}

class Login extends Component{
 render(){
  return (
       <div className="container1">
          <img  src={image3} alt="alt logo" width="100%"/>
          <h1 className="centered1">Login</h1>
          <button className="btn"><Link to="/ngo-login" className="nav-link">Ngo</Link></button> &emsp;
          <button className="btn1"><Link to="/parent-login" >Parent</Link></button>  
        </div>
  );
}
}

class Register extends Component{
  render(){
   return (
        <div className="container" >
           <img  src={image1} alt="alt logo" width="100%"/>
           <h1 className="centered">Register</h1>
           <button className="btn"><Link to="/ngo-register">Ngo</Link></button>
           <button className="btn1"><Link to="/parent-register">Parent</Link></button>
         </div>
   );
 }
 }

 const rootElement = document.getElementById("root");
 ReactDOM.render(<App />, rootElement);





    
 
 
