import React, { Component } from 'react'
import NgoService from '../../services/ngoService';
import ChildService from '../../services/childrenService';


class ListChildren extends Component {
        constructor(props) {
            super(props)
            this.state = {
                ngos: {},
                message: null
            }
          
            this.reloadNgo = this.reloadNgo.bind(this);
        }
    
        componentWillMount() {
            this.reloadNgo();
        }
    
       reloadNgo() {
            NgoService.getNgoByEmail(this.state.email=window.localStorage.getItem("email"))
            .then((res) => {
                console.log("email:",this.state.email);
                this.setState({ngos: res.data});  
                //console.log(this.state.ngos); 
            });
        }

        viewChild(ngo_id){
            window.localStorage.setItem("ngo_id",ngo_id)
            this.props.history.push(`/childListByNgo`);
            console.log(ngo_id);
        }

        deleteNgo(ngo_id){
            NgoService.deleteNgo(ngo_id).then( res => {
                console.log(ngo_id);
                this.setState({message : 'Ngo deleted successfully.'});
                this.setState({ngos:(ngo => ngo.ngo_id !== ngo_id)});
                console.log("deactivated!!!");
                this.props.history.push("/ngo-login");
            });
        }

        viewReq(ngo_id){
            window.localStorage.setItem("ngo_id",ngo_id)
            this.props.history.push(`/reqbyngoid`);
            console.log("in view Req",ngo_id);
        }
      
    render() {
        const {ngos} = this.state
        return (
          
            <div>
               
                 <h2 className="text-center">Ngo Information</h2>
                
                 <br></br>
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Ngo Id</th>
                                    <th> Ngo Name</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                   
                                      
                                        <tr key = {ngos.ngo_id}>
                                            <td> {ngos.ngo_id} </td>
                                            <td> {ngos.ngoName} </td>   
                                             <td> 
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewChild(ngos.ngo_id)} className="btn btn-info">Child List </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewReq(ngos.ngo_id)} className="btn btn-info">Request List </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteNgo(ngos.ngo_id)} className="btn btn-danger">Deactivate account </button>
                                             </td>
                                            
                                        </tr>
                                    
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListChildren;
