import React , { Component } from 'react';


import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import ChildrenComponent from '../children/AddChildren.jsx';
import LoginNgoComponent from "./LoginNgoComponent.js";
import ListChildren from '../children/viewChildrenList.jsx';
import viewChildById from '../children/viewChildrenById.jsx';
import updateChild from '../children/UpdateChildComponent.jsx';
import n1 from '../../images/n1.jpg';
import RequestList from "../request/RequestList.jsx";
import ViewReqById from "../request/ViewRequestById.jsx";
import Response from "../request/Response.jsx";
import ChildListByNgo from '../children/ViewChildByNgoId.jsx';
import NgoList from '../ngo/ngoList.jsx';
import NgoByEmail from './ngoByEmail.jsx';
import ReqByNgoId from '../request/ViewRequestByNgoId.jsx';
class NgoHomePage extends Component { 
    render(){
      return (
      <div >
      <BasicRouting></BasicRouting>
      </div>
      );
    }
}

export default NgoHomePage;

class BasicRouting extends Component{
    render(){
        return(
            <div>
                
                
                <Router> 
                <ul>
                <Link to="/ngo-home"><li><a class="active" href="# Home">Ngo Home</a></li></Link> 
                    <Link to="/child_list_view" ><li><a class="active" href="#Children List">Childrens List</a></li></Link>
                    <Link to="/parent"><li><a class="active" href="#Parent Request List">Parent Request List</a></li></Link>
                    <Link to="/NgoList"><li><a class="active" href="#Parent Request List">Child List</a></li></Link>
                    <Link to="/ngobyemail"><li><a class="active" href="#Parent Request List">Ngo</a></li></Link>
                    <Link to="/ngo-login"><li><a class="active" href="#Sign Out">Sign Out</a></li></Link>
             </ul>

                <Switch>
                    <Route path="/ngo-home" exact  component={Home}/>
                    <Route path="/child_add" component={ChildrenComponent} />
                    <Route path="/child_list_view" component={ListChildren} />     
                    <Route path="/viewChildById/:child_id" component={viewChildById} />  
                    <Route path="/updateChild/:child_id" component={updateChild} />     
                    <Route path="/parent"  component={RequestList}/>
                    <Route path="/ViewReqById/:req_id"  component={ViewReqById}/>
                    <Route path="/response/:req_id"  component={Response}/>
                    <Route path="/NgoList"  component={NgoList}/>
                    <Route path="/childListByNgo"  component={ChildListByNgo}/>
                    <Route path="/ngobyemail"  component={NgoByEmail}/>
                    <Route path="/ngo-login" component={LoginNgoComponent} />
                    <Route path="/reqbyngoid" component={ReqByNgoId} />
                    <Route path="/ngo-login" component={LoginNgoComponent}/>
                    
                </Switch>
                </Router> 
        </div>
        );
    }
    
}
class  Home extends Component{
    render(){
        return(
          <div className="container2">

              <div>
              <center>
                 <h3>Welcome to Ngo Home Page</h3>
              </center>
            
              </div>
           <div style={{marginLeft: "220px"}}>
            <img  src={n1} alt="alt logo" width="40%"/>

            <h3 className="centered3">“Be a part of the breakthrough and make someone’s dream come true..”</h3></div>
         </div>

        );
    }
}


class  Parent extends Component{
    render(){
        return(
       <div>
           <center>
             <h1>Welcome</h1> 
             </center>
                     </div>

        );
    }
}


