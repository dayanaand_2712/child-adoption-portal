import React, { Component } from 'react'
import NgoService from "../../services/ngoService";

class RegisterUserComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            phoneNumber: '',
            address: '',
            contactPerson: '',
            district: '',
            email: '',
            ngoName: '',
            password: '',
            setUpDate: '',
            state: ''
            
        }
        this.saveUser = this.saveUser.bind(this);
    }
    saveUser = (e) => {
        e.preventDefault();
        let user = {phoneNumber: this.state.phoneNumber,address: this.state.address, contactPerson: this.state.contactPerson,
             district: this.state.district, email: this.state.email,ngoName: this.state.ngoName,password: this.state.password,
             setUpDate: this.state.setUpDate,state:this.state.state};
             console.log(user);
        NgoService.addUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/ngo-login');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });
    render() {
        return( 
            <div className="register-user">
                <h2 className="text-center">Register Ngo</h2>
                <form>
                <div className="form-group">
                    <label>Ngo Name</label>
                    <input placeholder="Ngo Name" name="ngoName" className="form-control" value={this.state.ngoname} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>SetUP Date</label>
                    <input type="date" placeholder="SetUp Date" name="setUpDate" className="form-control" value={this.state.setUpDate} onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label>Address</label>
                    <input type="Address" placeholder=" Address" name="address" className="form-control" value={this.state.address} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>State</label>
                    <input type="State" placeholder=" State" name="state" className="form-control" value={this.state.state} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>District</label>
                    <input type="District" placeholder="District" name="district" className="form-control" value={this.state.district} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Contact Person </label>
                    <input type="Contact Person" placeholder="Contact Person" name="contactPerson" className="form-control" value={this.state.contactPerson} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Phone number</label>
                    <input type="text" placeholder="Phone number" name="phoneNumber" className="form-control" value={this.state.phoneNumber} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="Email" placeholder="Email" name="email" className="form-control" value={this.state.email} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="Password" placeholder="Password" name="password" className="form-control" value={this.state.password} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Confirm Password</label>
                    <input type="Confirm Password" placeholder="Confirm Password" name="confirmPassword" className="form-control" value={this.state.confirmPassword} onChange={this.onChange}/>
                </div>


                <button className="btn btn-success" onClick={this.saveUser}  >Register</button>
            </form>
    </div>
        );
    }
}

export default RegisterUserComponent;