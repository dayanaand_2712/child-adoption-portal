import React, { Component } from 'react'
import NgoService from '../../services/ngoService';
import ChildService from '../../services/childrenService';


class ListChildren extends Component {
    constructor(props) {
        super(props)

        this.state = {
                ngos: [],
                message: null
              
        }
      
    }
      
        componentDidMount(){
           NgoService.getNgo().then((res)=>{
               this.setState({ngos:res.data});
           });
        }
       
        viewChild(ngo_id){
            window.localStorage.setItem("ngo_id",ngo_id)
            this.props.history.push(`/childListByNgo`);
            console.log(ngo_id);
        }
      
    

    render() {
        return (
            <div>
               
                 <h2 className="text-center">Ngo Information</h2>
                
                 <br></br>
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Ngo Id</th>
                                    <th> Ngo Name</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.ngos.map(
                                        ngo => 
                                        <tr key = {ngo.ngo_id}>
                                            <td> {ngo.ngo_id} </td>
                                             <td> {ngo.ngoName} </td>   
                                             <td> 
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewChild(ngo.ngo_id)} className="btn btn-info">View </button>
                                             </td>
                                            
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListChildren;
