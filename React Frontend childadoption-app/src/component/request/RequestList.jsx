import React, { Component } from 'react'

import RequestService from '../../services/requestService';

class ListChildren extends Component {
    constructor(props) {
        super(props)

        this.state = {
                requests: [],
                message: null
              
        }
      
        this.response = this.response.bind(this);
       // this.updateChild = this.updateChild.bind(this);
        this.deleteRequest = this.deleteRequest.bind(this);
    }
      
        componentDidMount(){
           RequestService.getReqList().then((res)=>{
               this.setState({requests:res.data});
           });
        }
      
        
        response(){
            this.props.history.push("/response")
        }

        viewRequest(req_id){
            this.props.history.push(`/viewReqById/${req_id}`);
        }
       /* updateChild(child_id){
            this.props.history.push(`/updateChild/${child_id}`);
        }*/

        deleteRequest(req_id){
            RequestService.deleteReqById(req_id).then( res => {
                this.setState({message : 'Request deleted successfully.'});
                this.setState({requests: this.state.requests.filter(request => request.req_id !== req_id)});
            });
        }
    

    render() {
        return (
            <div>
               
                 <h2 className="text-center">Requests List</h2>
                 <br></br>
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Id</th>
                                    <th> Child Gender </th>
                                    <th> Child  Age</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.requests.map(
                                        request => 
                                        <tr key = {request.req_id}>
                                            <td> {request.req_id} </td>
                                            <td> {request.genderOfChild} </td> 
                                            <td> {request.ageOfChild}</td>
                                            
                                             <td>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewRequest(request.req_id)} className="btn btn-info">View </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteRequest(request.req_id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.response()} className="btn btn-info">Response </button>
                                             </td>
                                            
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListChildren;
