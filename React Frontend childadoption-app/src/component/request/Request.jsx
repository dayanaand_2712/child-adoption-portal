import React, { Component } from 'react'
import RequestService from "../../services/requestService";
class Request extends Component {

    constructor(props) {
        super(props);
        this.state = {

           // req_id: '',
            genderOfChild: '',
            healthStatusOfChild: '',
            ageOfChild: '',
            state: '',
           
             ngo:'',
              parent:'',
              child:'',
              moto: '',
            requested_date: '',
            
            statusOfRequest: ''
        }
        this.saveUser = this.saveUser.bind(this);
    }
    saveUser = (e) => {
        e.preventDefault();
        let request = {
           // req_id: this.state.req_id,
            genderOfChild: this.state.genderOfChild,
            healthStatusOfChild: this.state.healthStatusOfChild,
            ageOfChild: this.state.ageOfChild,
            state: this.state.state,
            moto: this.state.moto,
            requested_date: this.state.requested_date,
            statusOfRequest: this.state.statusOfRequest
        };
       
        let user = {
            request,
            ngoId: this.state.ngo,
            parentId: this.state.parent,
            childId: this.state.child
        };
    console.log(JSON.stringify(user));
   




RequestService.addUser(user)
    .then(res => {
        console.log(user);
        this.setState({ message: 'Request added successfully.' });
        this.props.history.push('/parent-home');
    });
    }

onChange = (e) =>
    this.setState({ [e.target.name]: e.target.value });
render() {
    return (
        <div className="register-user">
            <h2 className="text-center">Request</h2>
            <form>

               

                <div className="form-group">
                    <label>Gender of child</label>
                    <input placeholder="Gender of Child" name="genderOfChild" className="form-control" value={this.state.genderOfChild} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Health Of Child</label>
                    <input placeholder="Health Of Child" name="healthStatusOfChild" className="form-control" value={this.state.healthStatusOfChild} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Age of Child</label>
                    <input type="number" placeholder=" Age of Child" name="ageOfChild" className="form-control" value={this.state.ageOfChild} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>State</label>
                    <input placeholder="State" name="state" className="form-control" value={this.state.state} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>NGO ID</label>
                    <input type="ngo" placeholder="NGO ID" name="ngo" className="form-control" value={this.state.ngo} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Parent Id</label>
                    <input type="parent" placeholder="Parent Id" name="parent" className="form-control" value={this.state.parent} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Enter Child Id</label>
                    <input type="child" placeholder="Enter Child Id" name="child" className="form-control" value={this.state.child} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Moto</label>
                    <input placeholder="Enter Moto" name="moto" className="form-control" value={this.state.moto} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Enter Request Date</label>
                    <input type="date" placeholder="Enter Request Date" name="requested_date" className="form-control" value={this.state.requested_date} onChange={this.onChange} />
                </div>

                <div className="form-group">
                    <label>Status Of Request</label>
                    <input placeholder="Status Of Request" name="statusOfRequest" className="form-control" value={this.state.statusOfRequest} onChange={this.onChange} />
                </div>



                <button className="btn btn-success" onClick={this.saveUser} >Request</button>
            </form>
        </div>
    );
}
}

export default Request;