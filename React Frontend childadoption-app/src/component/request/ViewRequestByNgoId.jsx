import React, { Component } from 'react'
import RequestService from '../../services/requestService.js';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";

class ViewReqById extends Component {
    constructor(props) {
        super(props)
        this.state = {
            requests: [],
            message: null
        }
    
        this.reloadReqList = this.reloadReqList.bind(this);
        this.response = this.response.bind(this);
        // this.updateChild = this.updateChild.bind(this);
         this.deleteRequest = this.deleteRequest.bind(this);
    }

    componentWillMount() {
        this.reloadReqList();
    }

    reloadReqList() {
        RequestService.getReqByNgoId(this.ngo_id=window.localStorage.getItem('ngo_id'))
            .then((res) => {
                console.log("in req ngo number :",this.ngo_id);
                this.setState({requests: res.data})
                console.log(this.state.requests);
            });
    }
    viewRequest(req_id){
        this.props.history.push(`/viewReqById/${req_id}`);
    }
    response(req_id){
        this.props.history.push(`/response/${req_id}`);
    }

    deleteRequest(req_id){
        RequestService.deleteReqById(req_id).then( res => {
            this.setState({message : 'Request deleted successfully.'});
            this.setState({requests: this.state.requests.filter(request => request.req_id !== req_id)});
        });
    }

    render() {
        return (
            <div>
               
                 <h2 className="text-center">Requests List</h2>
                
                 <br></br>
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Request Id</th>
                                   
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.requests.map(
                                        request => 
                                        <tr key = { request.req_id}>
                                            <td> { request.req_id} </td>
                                            
                                           
                                             <td>
                                             <button style={{marginLeft: "10px"}} onClick={ () => this.viewRequest(request.req_id)} className="btn btn-info">Request Details </button>
                                             <button style={{marginLeft: "10px"}} onClick={ () => this.response(request.req_id)} className="btn btn-success">Response </button>
                                             <button style={{marginLeft: "10px"}} onClick={ () => this.deleteRequest(request.req_id)} className="btn btn-danger">Delete </button>
                                             </td>
                                            
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ViewReqById;
