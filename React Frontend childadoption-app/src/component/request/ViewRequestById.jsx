import React, { Component } from 'react'
import RequestService from '../../services/requestService';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";

class ViewReqById extends Component {
    constructor(props) {
        super(props)

        this.state = {
            req_id: this.props.match.params.req_id,
            request: {},
            child:{}
           
        }
    }

    componentDidMount(){
        RequestService.getReqById(this.state.req_id).then( res => {
            console.log(this.state.req_id);
            this.setState({request: res.data});
        })
        RequestService.getReqById(this.state.req_id).then( res => {
            console.log(this.state.req_id);
            this.setState({request: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> Request Details</h3>
                    <hr/>
                    <table>
                        <tr>
                            <td><label><b>Request ID :</b></label></td>
                            <td> {this.state.request.req_id}</td>
                        </tr>
                        <tr>
                            <td><label><b>Age of Child:</b></label></td>
                            <td> {this.state.request.ageOfChild}</td>
                        </tr>
                        <tr>
                            <td><label><b>Gender Of Child : </b></label></td>
                            <td> {this.state.request.genderOfChild}</td>
                        </tr>
                        <tr>
                            <td><label><b> Child Age : </b></label></td>
                            <td> {this.state.request.ageOfChild}</td>
                        </tr>
                        <tr>
                            <td>  <label><b>Health Information: </b></label> </td>
                            <td> { this.state.request.healthStatusOfChild}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> Reason :   </b></label> </td>
                            <td> { this.state.request.moto}</td>
                        </tr>
                       
                        <tr>
                            <td>  <label><b> Requested Date :</b></label></td>
                            <td> { this.state.request.requested_date}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> State :  </b></label> </td>
                            <td> { this.state.request.state}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> Child Id :  </b></label> </td>
                            <td> { this.state.child.child_id}</td>
                        </tr>
                       
                       
                    </table>
                  
                </div>
               
            </div>
        )
    }
}

export default ViewReqById;
