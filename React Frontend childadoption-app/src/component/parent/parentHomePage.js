import React , { Component } from 'react';
import parent4 from '../../images/parent4.jpg';
import '../../App.css';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import Eligibility from './Eligibility.jsx';
import Guidelines from './Guidlines.jsx';
import Document from './document.jsx';
import Already from './Already.jsx';
import Newly from './Newly';
import Request from '../request/Request.jsx';
import ParentChildrenList from './ChildrenListParent.jsx';
import viewChildList from '../children/viewChildrenById.jsx';
import viewchildparentbyid from './viewchildparentbyid.jsx';
import RegisterDonationComponent from '../Donation/RegisterDonationComponent.jsx';
import Homepage from '../Home/homePage.jsx';
import LoginParentComponent from '../parent/LoginParentComponent.jsx';
import PaymentForm from '../Donation/PaymentForm.jsx';
import Successfull from '../Donation/Feedback.jsx';
class ParentHomePage extends Component { 
    render(){
      return (
      <div >
      <BasicRouting></BasicRouting>
      </div>
      );
    }
}

export default ParentHomePage;

class BasicRouting extends Component{
    render(){
        return(
          <div>
              
            <div class="header">
                
            </div>
            <Router> 
              <ul>
                <Link to="/parent-home"><li><a class="active" href="# Home">Parent Home</a></li></Link> 
                <Link to="/childlistparent"><li><a class="active" href="#Childlistparent">Children List </a></li></Link>
                <Link to="/request"><li><a class="active" href="#Request">Request</a></li></Link>
                <Link to="/eligibility"><li><a class="active" href="#Eligibility Criteria">Eligibility Criteria</a></li></Link>
                <Link to="/guidelines"><li><a class="active" href="#Guidelines for Adoption">Guidelines for Adoption</a></li></Link>
                <Link to="/doctrequired"><li><a class="active" href="#Document Required">Document Required</a></li></Link>
                <Link to="/alreadyreg"><li><a class="active" href="#Instruction for Already Registered Parents">Instruction for Already Registered Parents</a></li></Link>
                <Link to="/newlyreg"><li><a class="active" href="#Instruction for Newly Registered Parent">Instruction for Newly Registered Parent</a></li></Link>
                <Link to="/payment"><li><a class="active" href="#Payment">Donation Payment</a></li></Link>
                <Link to="/parent-login"><li><a class="active" href="#Sign Out">Sign Out</a></li></Link>
             </ul>

                <Switch>
                    <Route path="/parent-home" exact  component={Home}/>
                    <Route path="/childlistparent" component={ParentChildrenList}/>
                    <Route path="/request"  component={Request}/>
                    <Route path="/eligibility"  component={Eligibility}/>
                    <Route path="/guidelines"  component={Guidelines}/>
                    <Route path="/doctrequired"  component={Document}/>
                    <Route path="/alreadyreg"  component={Already}/>
                    <Route path="/newlyreg"  component={Newly}/>
                    <Route path="/viewchildparentbyid/:child_id" component={viewchildparentbyid}/>
                    <Route path="/parent-login" component={LoginParentComponent}/>  
                    <Route path="/payment" component={RegisterDonationComponent}/>
                    <Route path="/PaymentForm" component={PaymentForm} />
                    <Route path="/Successfull" component={Successfull} />
                    <Route path="/" component={Homepage}/>
                </Switch>

            </Router>   
         </div>
        );
    }
}



class  Home extends Component{
    render(){
        return(
          <div className="container2">
              <center>
              <h3>Welcome to Parent Home Page</h3>
              </center>
                    <div style={{marginLeft: "220px"}}>
                            <img  src={parent4} alt="alt logo" width="100%"/>
                            <h3 className="centered2">“There are no unwanted children, just unfound families.”</h3>
                    </div>
            </div>
        );
    }
}




