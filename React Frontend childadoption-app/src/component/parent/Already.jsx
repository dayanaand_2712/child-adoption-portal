import React , { Component } from 'react';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
//import './parenthome.css';
import '../../App.css';


class  Already extends Component{
    render(){
        return(
            
     <div>
            <center>      
           
                <div  className="text">  
    
        <p>Dear Registered Parent,</p>
<ol>
    <li>
    All parents who have already registered and are in waiting list / HSR is completed/ approved, it is now mandatory to upload the relevant documents by login through your User ID and Password mentioned in the acknowledgement letter.The list of documents is mentioned below:
        </li>
     <li>Photograph of person/s adopting a child (Post Card Size)</li>
     <li>Birth Certificate</li>
<li>Proof of Residence (Adhaar Card/Voter Card/ Driving License/ Passport/ Current Electricity Bill/ Telephone Bill.</li>
<li>Proof of Income of last year (Salary Slip/ Income Certificate issued by Govt. Department/ Income Tax Return)</li>
<li>In case you are married, please upload Marriage Certificate</li>
<li>In case you are divorcee, please upload copy of Divorce Decree</li>
<li>In case of death of your spouse, please upload Death Certificate of spouse</li>
<li>Certificate from a medical practitioner certifying that the prospective adoptive parents(PAPs) do not suffer from any chronic, contagious or fatal disease and they are fit to adopt.</li>   
<li> Update your profile details including mobile number and email ID for any further communication.</li>
<li>In case you have forgotten your User ID/Password, you may retrieve them by clicking on the forgot password link available in Track Status page.</li>
<li>You may also contact your agency for uploading completed Home Study Report (HSR) which is mandatory</li>
<li>If you do not wish to continue with adoption process, you may withdraw your application by clicking on Withdraw option available in your login.</li>
<li>In case the above process of uploading the documents and Home Study Report (HSR) is not completed by 15th November 2014, your application will be assumed to be cancelled.</li>

<Link to="/doctrequired"> <button style={{marginRight: "550px"}}  className="btn btn-info">Prev </button></Link>

<Link to="/newlyreg"> <button style={{marginLeft: "450px"}}   className="btn btn-success">Next</button></Link> 


        </ol>       
            </div>

             </center>

               </div>
 

       );
   }
}
export default Already;