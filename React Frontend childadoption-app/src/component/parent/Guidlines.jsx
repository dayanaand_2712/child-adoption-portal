import React , { Component } from 'react';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
//import './parenthome.css';
import '../../App.css';

class  Guideline extends Component{
    render(){
        return(
       <div>
           <center>
            
            
                 <h3>
                     <center>
                 Guildlines Adoption Procedure</center>
                 </h3>
            
                 <div  className="text">  
                <ol>
                <li>
                 Registration and home study of the prospective adoptive parents:
                 The Indian prospective adoptive parents irrespective of their religion, if interested to adopt an orphan or abandoned or surrendered child, shall apply for the same to Specialised Adoption Agencies through Child Adoption Resource Information and Guidance System by filling up the online application form, as provided in Schedule VI, 
                 and uploading the relevant documents thereby registering themselves as prospective adoptive parents.</li>
                <li>The prospective adoptive parents shall opt for desired State or States by giving option for those particular States at the time of registration.</li>
                <li>
                Registration on Child Adoption Resource Information and Guidance System would be a deemed registration in all Specialised Adoption Agencies of the State or States they have opted for.
                </li>
                <li>
                The registration number of prospective adoptive parents shall be available with all the Specialised Adoption Agencies in those State or States, as the case may be.
                </li>
                <li>
                The registration shall be complete and confirmed to the prospective adoptive parents immediately on receipt of the completed application form and requisite documents on Child Adoption Resource Information and Guidance System:
Provided that the documents shall be uploaded within a period of thirty days from the date of registration failing which the prospective adoptive parents have to register afresh.
                </li>
                <li>
                The prospective adoptive parents shall get their registration number from the acknowledgement slip and use it for viewing the progress of their application.
                </li>
<li>
The prospective adoptive parents shall select a Specialised Adoption Agency nearest to their residence for Home Study Report in their State of habitual residence.
</li>
<li>
The Home Study Report of the prospective adoptive parents shall be prepared through the social worker of selected Specialised Adoption Agency and in case they are unable to conduct Home Study Report within stipulated time, they shall take the assistance of a social worker from a panel maintained by the State Adoption Resource Agency or District Child Protection Unit, as the case may be.
</li>
<li>
The Specialised Adoption Agency or the empanelled social worker of the State Adoption Resource Agency or District Child Protection Unit shall counsel the prospective adoptive parents during the home study.
</li>
<li>
The Home Study Report shall be completed in the format given in Schedule VII, within thirty days from the date of submission of requisite documents and shall be shared with the prospective adoptive parents immediately, thereafter.
</li>
<li>
The Home Study Report shall be posted in the Child Adoption Resource Information and Guidance System by the Specialised Adoption Agency as soon as it is complete.
</li>
<li>
The Home Study Report shall remain valid for three years and shall be the basis for adoption of a child by the prospective adoptive parents from anywhere in the country.
</li>
<Link to="/parent-home"> <button style={{marginRight: "550px"}}  className="btn btn-info">Prev </button></Link>

 <Link to="/eligibility"> <button style={{marginLeft: "450px"}}   className="btn btn-success">Next</button></Link>
                    </ol>

             </div>
             </center>
                     </div>

        );
    }
}
export default Guideline;