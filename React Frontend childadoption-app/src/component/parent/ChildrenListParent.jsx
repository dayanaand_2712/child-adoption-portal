import React, { Component } from 'react'
import NgoService from '../../services/ngoService';


class ChildrenListParent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                childs: [],
                message: null
              
        }
       
        
    }
      
        componentDidMount(){
           NgoService.getChild().then((res)=>{
               this.setState({childs:res.data});
           });
        }
       
        viewChild(child_id){
            this.props.history.push(`/viewchildparentbyid/${child_id}`);
        }
      
         

    render() {
        return (
            <div>
               
                 <h2 className="text-center">Childrens List</h2>
                
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Child Id</th>
                                    <th> Child Name</th>
                                    <th> Child BirthDate</th>
                                    <th> Child  Age</th>
                                    <th> Child Gender </th>
                                    <th> Child Color</th>
                                    <th> Child  health Info</th>
                                    <th> Child  Blood Group</th>
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.childs.map(
                                        child => 
                                        <tr key = {child.child_id}>
                                            <td> {child.child_id} </td>
                                             <td> {child.child_name} </td>   
                                             <td> {child.birthDate}</td>
                                             <td> {child.age}</td>
                                             <td> {child.gender} </td>   
                                             <td> {child.color} </td>
                                             <td> {child.health}</td>
                                             <td> {child.bloodGroup}</td>
                                           
                                             <td>
                                                
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewChild(child.child_id)} className="btn btn-info">View </button>
                                             </td>
                                            
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ChildrenListParent;
