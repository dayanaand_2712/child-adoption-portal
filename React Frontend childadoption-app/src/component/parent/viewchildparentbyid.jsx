import React, { Component } from 'react'
import ChildService from '../../services/childrenService';

class viewchildparentbyid extends Component {
    constructor(props) {
        super(props)

        this.state = {
            child_id: this.props.match.params.child_id,
            child: {}
        }
    }

    componentDidMount(){
        ChildService.getChildById(this.state.child_id).then( res => {
            this.setState({child: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Childrens Details</h3>
                    <hr/>
                    <table>
                        <tr>
                            <td><label><b>Child ID :</b></label></td>
                            <td> {this.state.child.child_id}</td>
                        </tr>
                        <tr>
                            <td><label><b>Name :</b></label></td>
                            <td> {this.state.child.child_name}</td>
                        </tr>
                        <tr>
                            <td><label><b>Child BirthDate : </b></label></td>
                            <td> {this.state.child.birthDate}</td>
                        </tr>
                        <tr>
                            <td><label><b> Child Age : </b></label></td>
                            <td> {this.state.child.age}</td>
                        </tr>
                        <tr>
                            <td>  <label><b>Gender: </b></label> </td>
                            <td> { this.state.child.gender}</td>
                        </tr>
                        <tr>
                            <td>  <label><b>Child Color :  </b></label> </td>
                            <td> { this.state.child.color}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> Health information : </b></label></td>
                            <td> { this.state.child.health}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> Blood Group :   </b></label> </td>
                            <td> { this.state.child.bloodGroup}</td>
                        </tr>
                        <tr>
                            <td>  <label><b> Ngo Information:   </b></label> </td>
                            <td> { this.state.child.ngo}</td>
                        </tr>
                    </table>
                  
                </div>
            </div>
        )
    }
}

export default viewchildparentbyid;
