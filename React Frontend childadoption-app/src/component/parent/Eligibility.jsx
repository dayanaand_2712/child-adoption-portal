import React , { Component } from 'react';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
//import './parenthome.css';
import '../../App.css';
class  Eligibility extends Component{
    render(){
        return(
            <div>
                <center>
                <h3>Eligibility criteria for prospective adoptive parents</h3>
              
               <div  className="text">  
          
                <ol>
                <li>1. The prospective adoptive parents shall be physically, 
                mentally and emotionally stable, 
                financially capable and shall not have any life threatening medical condition.</li> 
                <li>2. Any prospective adoptive parents, irrespective of his marital status and whether or not he 
            has biological son or daughter, can adopt a child subject to following, namely:-</li>
<li>a.the consent of both the spouses for the adoption shall be required, in case of a married couple;</li>
<li>b.a single female can adopt a child of any gender;</li>
<li>c.a single male shall not be eligible to adopt a girl child;</li>
                <li>3.No child shall be given in adoption
                     to a couple unless they have at least two years of stable marital relationship.</li>
                <li>4.The age of prospective adoptive parents, as on the date of registration,  
    shall be counted for deciding the eligibility and 
    the eligibility of prospective adoptive parents to apply for children of different age groups shall be as under:-
</li>
    <li>5.In case of couple, the composite age of the prospective adoptive parents shall be counted.</li>
 <li>6.The minimum age difference between the child and either of the prospective adoptive parents shall not be less than twenty-five years.</li>
 <li>7.The age criteria for prospective 
     adoptive parents shall not be applicable in case of relative adoptions and adoption by step-parent.</li>
<li>8.Couples with three or more children shall not be considered for adoption except in case of special need 
    children as defined in sub-regulation (21) of regulation 2, hard to place children as mentioned in regulation 50 and in case of relative adoption and adoption by step-parent.</li>
    <Link to="/guidelines"> <button style={{marginRight: "550px"}}  className="btn btn-info">Prev </button></Link>

<Link to="/doctrequired"> <button style={{marginLeft: "450px"}}   className="btn btn-success">Next</button></Link> 
                
                </ol>
           
                </div>
            </center>
        </div>


        );
    }
}
export default Eligibility;