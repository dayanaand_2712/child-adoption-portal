import React, { Component } from 'react'
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import ParentService from "../../services/parentService";
class RegisterUserComponent extends Component{

    constructor(props){
        super(props);
        this.state ={

            address: '',
            adharNumber: '',
            city: '',
            district: '',
            email:'',
            fIncome:'',
            fOccupation: '',
            femaleParAge: '',
            femaleParName: '',
            gender:'',
            mIncome: '',
            mOccupation: '',
            maleParAge: '',
            maleParName: '',
            maritalStatus: '',
            mobileNumber: '',
            numOfChildren: '',
            numOfchildrenParentHave: '',
            password: '',
            pinCode: '',
            reg_date: '',
            state: ''    
        }
        this.saveUser = this.saveUser.bind(this);
    }
    saveUser = (e) => {
        e.preventDefault();
        let user = {address: this.state.address, 
            adharNumber: this.state.adharNumber,
            city: this.state.city, 
            district: this.state.district,
             email: this.state.email, 
             fIncome: this.state.fIncome,
            fOccupation: this.state.fOccupation,
             femaleParAge: this.state.femaleParAge,
            femaleParName: this.state.femaleParName,
            gender: this.state.gender,
            mIncome: this.state.mIncome,
            mOccupation: this.state.mOccupation,
             maleParAge: this.state.maleParAge,
            maleParName: this.state.maleParName,
            maritalStatus:this.state.maritalStatus,
            mobileNumber:this.state.mobileNumber,
            numOfChildren:this.state.numOfChildren,
            numOfchildrenParentHave:this.state.numOfchildrenParentHave,
            password: this.state.password,
            pinCode: this.state.pinCode,
            reg_date:this.state.reg_date,
            state:this.state.state
        };
           
        ParentService.addUser(user)
            .then(res => {
                this.setState({message : 'Parent added successfully.'});
                this.props.history.push('/parent-login');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });
    render() {
        return( 
            <div className="register-user">
                <h2 className="text-center">Register Parent</h2>
                <form>
                    
                <div className="form-group">
                    <label>Gender</label>
                    <input type="Gender" placeholder=" Gender" name="gender" className="form-control" value={this.state.gender} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Male Parent Name</label>
                    <input placeholder="Male Parent Name" name="maleParName" className="form-control" value={this.state.maleParName} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Female Parent Name</label>
                    <input placeholder="Female Parent Name" name="femaleParName" className="form-control" value={this.state.femaleParName} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Male Parent Age</label>
                    <input type="Male Parent Age" placeholder=" Male Parent Age" name="maleParAge" className="form-control" value={this.state.maleParAge} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Female Parent Age</label>
                    <input type="Female Parent Age" placeholder="Female Parent Age" name="femaleParAge" className="form-control" value={this.state.femaleParAge} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter male Parent Occupation</label>
                    <input type="Enter male Parent Occupation" placeholder="Enter male Parent Occupation" name="mOccupation" className="form-control" value={this.state.mOccupation} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter female Parent Occupation</label>
                    <input type="Enter female Parent Occupation" placeholder="Enter female Parent Occupation" name="fOccupation" className="form-control" value={this.state.fOccupation} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter male Parent Income</label>
                    <input type="Enter male Parent Income" placeholder="Enter male Parent Income" name="mIncome" className="form-control" value={this.state.mIncome} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter female Parent Income</label>
                    <input type="Enter female Parent Income" placeholder="Enter female Parent Income" name="fIncome" className="form-control" value={this.state.fIncome} onChange={this.onChange}/>
                </div>
                
                <div className="form-group">
                    <label>Enter Number Of Children wants to adopt</label>
                    <input type="Enter Number Of Children wants to adopt" placeholder="Enter Number Of Children wants to adopt" name="numOfChildren" className="form-control" value={this.state.numOfChildren} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter Number Of Children Parent Have</label>
                    <input type="Enter Number Of Children Parent Have" placeholder="Enter Number Of Children Parent Have" name="numOfchildrenParentHave" className="form-control" value={this.state.numOfchildrenParentHave} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Address</label>
                    <input type="Address" placeholder="Address" name="address" className="form-control" value={this.state.address} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>City</label>
                    <input type="City" placeholder="City" name="city" className="form-control" value={this.state.city} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>District</label>
                    <input type="District" placeholder="District" name="district" className="form-control" value={this.state.district} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter State</label>
                    <input type="Enter State" placeholder="State" name="state" className="form-control" value={this.state.state} onChange={this.onChange}/>
                </div>
                
                <div className="form-group">
                    <label>Enter Pin Code</label>
                    <input type="Enter Pin Code" placeholder="Pin Code" name="pinCode" className="form-control" value={this.state.pinCode} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter Adhar Number</label>
                    <input type="number" placeholder="Adhar Number" name="adharNumber" className="form-control" value={this.state.adharNumber} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Enter Phone Number</label>
                    <input type="number" placeholder="Phone Number" name="mobileNumber" className="form-control" value={this.state.mobileNumber} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Marital Status</label>
                    <input type="Marital Status" placeholder="Marital State" name="maritalStatus" className="form-control" value={this.state.maritalStatus} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Registration Date</label>
                    <input type="date" placeholder="Registration Date" name="reg_date" className="form-control" value={this.state.reg_date} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Email id</label>
                    <input type="Email id" placeholder="Email id" name="email" className="form-control" value={this.state.email} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="Password" placeholder="Password" name="password" className="form-control" value={this.state.password} onChange={this.onChange}/>
                </div>


                <button className="btn btn-success" onClick={this.saveUser} >Register</button>
            </form>
    </div>
        );
    }
}

export default RegisterUserComponent;