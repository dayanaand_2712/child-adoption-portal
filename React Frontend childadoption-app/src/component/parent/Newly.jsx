import React , { Component } from 'react';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
//import './parenthome.css';
import '../../App.css';


class  Newly extends Component{
    render(){
        return(
            <div>
            <center>      
            
                <div  className="text">  
              
        <p>Dear Newly Registering Parent,</p>
        
        <ol>
            <li>
        Kindly read these instructions carefully before registering online:
        </li>
        This registration is meant for Indian citizens residing in India

        <li>
        If one of the PAPs is a foreigner and other is an Indian, please go to the page Register Online and select appropriate columns in Nationality
        </li>
        <li>
        Please give your correct residential address and telephone no. with area code
        </li>
        <li>You or your spouse must have a Permanent Account Number (PAN) card and you have to upload PAN card in portable document format (.pdf) – size should not exceed 512 KB</li>
        <li>
        You have to upload your (single parent) or your family photograph (couple) in .jpg format (3.5 x 4.5 cm). – Size should not exceed 1 MB
        </li>
        <li>
        You must have an email account and mobile number
        </li>
        <li>
        After successful registration, you will receive an online acknowledgement letter which will contain your registration and credential details
        </li>
        <li>
        In case you misplace your online acknowledgement letter, then it can be regenerated using Forgot Password link available in Track Status page
        </li>
        <li>
        Please upload the following documents:
        </li>
        <li>
        Photograph of person/s adopting a child (Post Card Size)
        </li>
        <li>
        Birth Certificate</li>
<li>Proof of Residence (Adhaar Card/Voter Card/ Driving License/ Passport/ Current Electricity Bill/ Telephone Bill</li>
<li>Proof of Income of last year (Salary Slip/ Income Certificate issued by Govt. Department/ Income Tax Return)</li>
<li>In case you are married, please upload Marriage Certificate</li>
<li>In case you are divorcee, please upload copy of Divorce Decree</li>
<li>
Certificate from a medical practitioner certifying that the PAPs do not suffer from any chronic, contagious or fatal disease and they are fit to adopt.
</li>
<Link to="/alreadyreg"> <button style={{marginRight: "550px"}}  className="btn btn-info">Prev </button></Link>

<Link to="/parent-home"> <button style={{marginLeft: "450px"}}   className="btn btn-success">Next</button></Link>  
</ol>       
            </div>

             </center>

               </div>
 

       );
   }
}
export default Newly;