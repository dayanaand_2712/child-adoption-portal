import React, { Component } from 'react'
import ChildrenService from "../../services/childrenService"

class UpdateChildComponent extends Component {

    constructor(props){
        super(props);
        this.state ={
           child_id: '',
            child_name: '',
            birthDate:'',
            age: '',
            gender: '',
            color: '',
            health: '',
            bloodGroup: ''
        }
        this.saveChild = this.saveChild.bind(this);
        this.loadChild = this.loadChild.bind(this);
    }

    componentDidMount() {
        this.loadChild();
    }

    loadChild() {
        ChildrenService.getChildById(window.localStorage.getItem("child_id"))
            .then((res) => {
                let child = res.data.result;
                this.setState({
                                child_id:child.child_id,
                                child_name:child.child_name,   
                                birthDate:child.birthDate,
                                age:child.age,
                                gender: child.gender,  
                                color:child.color,
                                health: child.health,
                                bloodGroup:child.bloodGroup
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveChild = (e) => {
        e.preventDefault();
         let child = {child_id: this.state.child_id,child_name: this.state.child_name,birthDate: this.state.birthDate,
             age: this.state.age,
             gender: this.state.gender, color: this.state.color,health: this.state.health,
             bloodGroup: this.state.bloodGroup};
        ChildrenService.updateChild(child)
            .then(res => {
                this.setState({message : 'child updated successfully.'});
                this.props.history.push('/child_list_view');
            });
    }

    render() {
        return (
            <div className="register-user">
                <h2 className="text-center">Edit User</h2>
                <form>

                    <div className="form-group">
                        <label>Children Id</label>
                        <input type="number" placeholder="Children Id" name="child_id" className="form-control" value={this.state.child_id} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Children Name</label>
                        <input type="Children Name" placeholder="Children Name" name="child_name" className="form-control" value={this.state.child_name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Children BirthDate</label>
                        <input type="date" placeholder="Children BirthDate" name="birthDate" className="form-control" value={this.state.birthDate} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Age</label>
                        <input type="number" placeholder=" Age" name="age" className="form-control" value={this.state.age} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Gender</label>
                        <input type="Gender" placeholder=" Gender" name="gender" className="form-control" value={this.state.gender} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Color</label>
                        <input type="text" placeholder="Color" name="color" className="form-control" value={this.state.color} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Children Health Information </label>
                        <input type="Children health" placeholder="Children health" name="health" className="form-control" value={this.state.health} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Blood Group</label>
                        <input type="Blood Group" placeholder="Blood Group" name="bloodGroup" className="form-control" value={this.state.bloodGroup} onChange={this.onChange}/>
                    </div>
                    <button className="btn btn-success" onClick={this.saveChild}>Save</button>
                </form>
            </div>
        );
    }
}

export default UpdateChildComponent;