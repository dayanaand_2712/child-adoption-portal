import React, { Component } from 'react'
import ChildService from "../../services/childrenService";

class ChildrenComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            //child_id: '',
            child_name: '',
            birthDate:'',
            age: '',
            gender: '',
            color: '',
           // imageUrl:'',
            health: '',
            bloodGroup: '',
            ngo:''
        }
        this.saveChild = this.saveChild.bind(this);
    }
    saveChild = (e) => {
        e.preventDefault();
        let child = {//child_id: this.state.child_id,
            child_name: this.state.child_name,
            birthDate: this.state.birthDate,
             age: this.state.age,
             gender: this.state.gender, color: this.state.color,/* imageUrl: this.state.imageUrl,*/
             health: this.state.health,
             bloodGroup: this.state.bloodGroup,
            };

            let child1={
                child,
                ngoId:this.state.ngo,
               // parentId:this.state.parent
            }
            console.log(JSON.stringify(child1));
            // console.log(child);
             ChildService.addChild(child1)
            .then(res => {
                console.log(child1);
                this.setState({message : 'child added successfully.'});
                this.props.history.push('/child_list_view');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });
    render() {
        return( 
            <div className="register-user">
                <h2 className="text-center">Register child</h2>
                <form>
                   

                    <div className="form-group">
                        <label>Children Name</label>
                        <input type="Children Name" placeholder="Children Name" name="child_name" className="form-control" value={this.state.child_name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Children BirthDate</label>
                        <input type="date" placeholder="Children BirthDate" name="birthDate" className="form-control" value={this.state.birthDate} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Age</label>
                        <input type="number" placeholder=" Age" name="age" className="form-control" value={this.state.age} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Gender</label>
                        <input type="Gender" placeholder=" Gender" name="gender" className="form-control" value={this.state.gender} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Color</label>
                        <input type="text" placeholder="Color" name="color" className="form-control" value={this.state.color} onChange={this.onChange}/>
                    </div>

                  
               

                    <div className="form-group">
                        <label>Children Health Information </label>
                        <input type="Children health" placeholder="Children health" name="health" className="form-control" value={this.state.health} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Blood Group</label>
                        <input type="text" placeholder="Blood Group" name="bloodGroup" className="form-control" value={this.state.bloodGroup} onChange={this.onChange}/>
                    </div>
                    <div className="form-group">
                        <label>Ngo Id</label>
                        <input type="number" placeholder="Ngo Id" name="ngo" className="form-control" value={this.state.ngo} onChange={this.onChange}/>
                    </div>


                <button className="btn btn-success" onClick={this.saveChild}>Register</button>
            </form>
    </div>
        );
    }
}

export default ChildrenComponent;