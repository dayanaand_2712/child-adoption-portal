import React, { Component } from 'react'
import NgoService from '../../services/ngoService';
import ChildService from '../../services/childrenService';

class ListChildren extends Component {
        constructor(props) {
            super(props)
            this.state = {
                childs: [],
                message: null
            }
            this.addChild = this.addChild.bind(this);
            this.updateChild = this.updateChild.bind(this);
            this.deleteChild = this.deleteChild.bind(this);
        
           
            this.reloadChildList = this.reloadChildList.bind(this);
        }
    
        componentWillMount() {
            this.reloadChildList();
        }
    
        reloadChildList() {
            ChildService.getChildByNgoId(this.ngo_id=window.localStorage.getItem('ngo_id'))
                .then((res) => {
                    console.log("ngo number :");
                    this.setState({childs: res.data})
                    console.log(this.state.childs);

                });
             
        }
    
    


        addChild(){
            this.props.history.push('/child_add');
        }
        viewChild(child_id){
            this.props.history.push(`/viewChildById/${child_id}`);
        }
        updateChild(child_id){
            this.props.history.push(`/updateChild/${child_id}`);
        }

        deleteChild(child_id){
            ChildService.deleteChild(child_id).then( res => {
                this.setState({message : 'Child deleted successfully.'});
                this.setState({childs: this.state.childs.filter(child => child.child_id !== child_id)});
            });
        }
    

    render() {
        return (
            <div>
               
                 <h2 className="text-center">Childrens List</h2>
                 <center>
                 <button className="btn btn-primary" onClick={this.addChild}> Add Children</button>
                 </center>
                 <br></br>
                 <div style={{marginLeft: "265px"}}>
  
                        <table className = "table table-striped table-bordered" marginLeft="10px">
                            <thead>
                                <tr>
                                    <th> Child Id</th>
                                    <th> Child Name</th>
                                    <th> Child BirthDate</th>
                                    <th> Child  Age</th>
                                    <th> Child Gender </th>
                                    <th> Child Color</th>
                                    <th> Child  health Info</th>
                                    <th> Child  Blood Group</th>
                                  
                                    <th> Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.childs.map(
                                        child => 
                                        <tr key = {child.child_id}>
                                            <td> {child.child_id} </td>
                                             <td> {child.child_name} </td>   
                                             <td> {child.birthDate}</td>
                                             <td> {child.age}</td>
                                             <td> {child.gender} </td>   
                                             <td> {child.color} </td>
                                             <td> {child.health}</td>
                                             <td> {child.bloodGroup}</td>
                                         
                                           
                                             <td>
                                                 <button onClick={ () => this.updateChild(child.child_id)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteChild(child.child_id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewChild(child.child_id)} className="btn btn-info">View </button>
                                             </td>
                                            
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListChildren;
