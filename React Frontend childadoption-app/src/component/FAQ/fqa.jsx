import React, { Component } from 'react';
import Faq from "react-faq-component";


const data = {
 title: "FAQ's (How it works)",
  rows: [
      {
        title : "What is Adoption?",
        content : "Adoption means the process through which the adopted child is permanently separated from his biological parents and becomes the lawful child of the adoptive parents with all the rights, privileges and responsibilities that are attached to a biological child. "
      },
      {
          title : "I want to adopt a newborn child. Can I adopt?",
          content : "No, newborn child cannot be adopted. Adoption of every child requires them to be declared legally free for adoption through Child Welfare Committee under Juvenile Justice (Care and Protection) Act,2015. The process usually takes at least two months to complete. Therefore, no new born child can be placed in adoption prior to completion of the process."
      },
    {
      title: "My friend has adopted a child directly from hospital. Is it legal?",
      content: "Our office hours are Monday through Friday, 9AM through 5PM Eastern time."
    },
    {
      title: "How Long Does It Take?",
      content: "No, the adoption directly from hospital is an illegal adoption and not permitted under law. There are penal provisions of imprisonment and fine specified under Section 80 of the Juvenile Justice Act 2015."
    },
    {
        title : "Can a person of any religion or caste adopt?",
        content : "Yes, there are no restrictions based on caste or religion under the JJ (C&PC) Act 2015."
    },
  
    {
        title: "Who Is Eligible For The Adoption Tax Credit?",
        content: "The adoption tax credit is a direct write-off of taxes owed, and therefore much more valuable than a deduction. Employer-paid adoption assistance of up to $14,300 per child is also tax-free: 1)If your modified Adjusted Gross Income (AGI) is $214,520 or less, you qualify for the entire credit If your modified Adjusted Gross Income (AGI) is $214,520 or less, you qualify for the entire credit."
      
    },
    {
        title : "I want to adopt a newborn child. Can I adopt?",
        content : "No, newborn child cannot be adopted. Adoption of every child requires them to be declared legally fre for adoption through Child Welfare Committee under Juvenile Justice (Care and Protection) Act,2015. The process usually takes at least two months to complete. Therefore, no new born child can be placed in adoption prior to completion of the process."
        
        },
      {
        title: "What Financing Options Are Available?",
        content: "Our services include a review of various funding options including grants, loans and fund-raising concepts. The right combination will be different for each person, but there are many financial options, including:Home Equity or Home Refinancing: Usually carries the lowest percentage rate. Also, the interest on the loan is usually tax-deductible.Employer Contributions: Check with your Human Resources department regarding any benefits your employer may have to partially defray adoption expenses.Private Loans: Talk to your banker or your financial lending institutions about the availability of special loans.Retirement Borrowing: Check with your retirement plan regarding borrowing from your retirement savings, then repaying yourself with interest."
      },
      {
        title : "Do we have to disclose the information about adoption, when the child gets married?",
        content : "It is advisable that this information be disclosed."
      },


      {
          title :"Do we have to inform the school that the child is an adopted child?",
          content : "It is not a legal requirement to inform the school that the child is an adopted child."
      },
      {
        title: " Can biological parents come back for their child? ",
        content: "No, once the adoption order has been granted, the biological parents have no legal ties with the child."       
      },
    {
      title: "How Much Should I Expect To Spend?",
      content: "Most adoptions range from $35,000 to high-$40,000s. However, federal and employer assistance is often available and you do not need the full amount up front to begin your adoption."
    }

]
}

export default class App extends Component {
  render() {
    return (
       
       
      <div>
         <Faq data={data}/>
       
      </div>
      
    )
  }
}
