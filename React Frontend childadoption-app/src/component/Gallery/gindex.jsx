import React , { Component } from 'react';
import ImageGallery from 'react-image-gallery';
//import "react-image-gallery/styles/css/image-gallery.css";
//import a1 from '../../images/kids.png';

const images = [
 
  {
    original: 'https://picsum.photos/id/1066/1000/600/',
    thumbnail: 'https://picsum.photos/id/1066/250/150/',
  },

  {
    original : 'https://image.shutterstock.com/image-vector/lgbt-family-flat-vector-illustration-260nw-1806886828.jpg',
    thumbnail : 'https://image.shutterstock.com/image-vector/lgbt-family-flat-vector-illustration-260nw-1806886828.jpg',
  },
  {
    original: 'https://picsum.photos/id/838/1000/600/',
    thumbnail: 'https://picsum.photos/id/838/250/150/',
  },
  {
    original : 'https://www.thestatesman.com/wp-content/uploads/2020/03/adopt.jpg',
    thumbnail : 'https://www.thestatesman.com/wp-content/uploads/2020/03/adopt.jpg',
  },
 
{
 original : 'https://image.shutterstock.com/image-photo/father-little-kids-outdoors-on-260nw-1088995808.jpg',
 thumbnail : 'https://image.shutterstock.com/image-photo/father-little-kids-outdoors-on-260nw-1088995808.jpg',
},

{
 original : ' https://i.pinimg.com/236x/d4/8d/28/d48d289d93e6867982d3c1282cad7cd0--beautiful-stories-beautiful-pictures.jpg',
 thumbnail : '  https://i.pinimg.com/236x/d4/8d/28/d48d289d93e6867982d3c1282cad7cd0--beautiful-stories-beautiful-pictures.jpg',
},
{
  original : 'https://www.villagesquare.in/wp-content/uploads/2020/08/Nalbari.jpg',
  thumbnail : 'https://www.villagesquare.in/wp-content/uploads/2020/08/Nalbari.jpg',
},
{
  original : 'https://images.hindustantimes.com/rf/image_size_630x354/HT/p2/2020/12/16/Pictures/_b660ee78-3f9b-11eb-be7c-3ee0679f75c0.jpg',
  thumbnail : 'https://images.hindustantimes.com/rf/image_size_630x354/HT/p2/2020/12/16/Pictures/_b660ee78-3f9b-11eb-be7c-3ee0679f75c0.jpg',
},
{
  original :'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSuCvY01W0odlrA4BPO_wwycwSTiGCBz6OnQ&usqp=CAU.jpg',
  thumbnail : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSuCvY01W0odlrA4BPO_wwycwSTiGCBz6OnQ&usqp=CAU.jpg',
},
{
 
  original: 'https://picsum.photos/id/822/1000/600/',
  thumbnail: 'https://picsum.photos/id/822/250/150/',
},
{
  original : 'https://bhartifoundation.org/wp-content/uploads/2018/12/Bharti-Foundation-Among-Best-NGO-In-India.jpg',
  thumbnail : 'https://bhartifoundation.org/wp-content/uploads/2018/12/Bharti-Foundation-Among-Best-NGO-In-India.jpg',
},
{
  original : 'https://thelogicalindian.com/h-upload/2020/01/27/152340-10-1.jpg',
  thumbnail : 'https://thelogicalindian.com/h-upload/2020/01/27/152340-10-1.jpg',
},
{
  original : 'https://www.stjosephstechnology.ac.in/web/img/ngo/1-1.png',
thumbnail : 'https://www.stjosephstechnology.ac.in/web/img/ngo/1-1.png',
}

];

class MyGallery extends Component {
  render() {
    return <ImageGallery items={images} />;
  }
}

export default MyGallery;