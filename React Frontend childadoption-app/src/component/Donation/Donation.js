import React , { Component } from 'react';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import PaymentForm from './PaymentForm.jsx';
//import './parenthome.css';
import '../../App.css';
import Donationhome from './DonationHome.jsx';
import Healthdonation from './Healthdonation.jsx';
import EducationDonation from './EducationDonation.jsx';
import FoodDonation from './FoodDonation.jsx';
import OtherDonation from './OtherDonation.jsx';
import Successfull from './Feedback.jsx';
import image1  from '../../images/don.jpg';
class Donation extends Component { 
    render(){
      return (
      <div >
      <BasicRouting></BasicRouting>
      </div>
      );
    }
}

export default Donation;
class BasicRouting extends Component{
    render(){
        return(
          <div>
              
            <div class="header">
                
            </div>  
               
    <Router> 
    <ul>
    <Link to="/donation"><li><a class="active" href="# Donation">Donation</a></li></Link> 
    <Link to="/donation-home"><li><a class="active" href="# Home">Donation Home</a></li></Link> 
    <Link to="/donation-education"><li><a class="active" href="# Education">Education Donation </a></li></Link> 
    <Link to="/donation-health"><li><a class="active" href="# Health">Medical Donation</a></li></Link> 
    <Link to="/donation-food"><li><a class="active" href="# Food">Food Donation</a></li></Link> 
    <Link to="/donation-other"><li><a class="active" href="# Other">Other Donation</a></li></Link> 
     <Link to="/PaymentForm" ><li><a class="active" href="#Payment">Payment Form</a></li></Link>
       
    
        
 </ul>

    <Switch>
        <Route path="/donation" exact  component={Home}/>
        <Route path="/donation-home" exact  component={Donationhome}/>
        <Route path="/donation-education" exact  component={EducationDonation}/>
        <Route path="/donation-health" exact  component={Healthdonation}/>
        <Route path="/donation-food" exact  component={FoodDonation}/>
        <Route path="/donation-other" exact  component={OtherDonation}/>
        <Route path="/PaymentForm" component={PaymentForm} />
        <Route path="/successfull" component={Successfull} />
       
        


        

    </Switch>
    </Router> 

</div>



        );
    }
}

class  Home extends Component{
    render(){
        return(
          <div className="text17">
              <center>
              <h1>Donation</h1>
          

</center>
<div style={{marginLeft: "265px"}}>
            <img  src={image1} alt="alt logo" width="100%"/>
           
         </div>

         <Link to="/donation-home"> <button style={{marginLeft: "265px"}}   className="btn btn-danger">Next</button></Link>
</div>

        );
    }
}