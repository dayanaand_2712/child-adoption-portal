import React , { Component } from 'react';
import { Link } from 'react-router-dom';


//import './parenthome.css';
import '../../App.css';


class Education extends Component{
    render(){
        return(
            <div>
            <div  className="text13">     
            <div>
 
<h1>Education Donation</h1>
<hr/>
<ol>
Smile Foundation, an NGO for poor child education, is an NGO in India directly benefitting over 15,00,000 children
 and their families every year, through more than 400 live welfare projects on education, healthcare, livelihood and women empowerment,
 in over 2000 remote villages and slums across 25 states of India.</ol>

<ol>Education is both the means as well as the end to a better life:
 the means because it empowers an individual to earn his/her livelihood and the end because it increases one's awareness on a range of issues – from healthcare to appropriate social behaviour to understanding one's rights – and in the process  help him/her evolve as a better citizen.</ol>
<ol>Doubtless, education is the most powerful catalyst for social transformation.
 But child education cannot be done in isolation. </ol>
 <ol>A child will go to school only if the family, particularly the mother, 
 is assured of healthcare and empowered.</ol>
 <ol> Moreover, when an elder sibling is relevantly skilled to be employable 
 and begins earning, </ol>
 <ol>the journey of empowerment continues beyond the present generation.
</ol>

 
</div>
<Link to="/donation-home"> <button style={{marginRight: "950px"}}  className="btn btn-info">Prev </button></Link>
 <Link to="/PaymentForm"> <button style={{marginLeft: "650px"}}   className="btn btn-success">Donate </button></Link>
 <Link to="/donation-health"> <button style={{marginRight: "350px"}}   className="btn btn-danger">Next</button></Link>
        </div>
        </div>
        
        );
        }
        }
        export default Education;