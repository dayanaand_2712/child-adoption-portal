import React , { Component } from 'react';
import { Link } from 'react-router-dom';


//import './parenthome.css';
import '../../App.css';


class  Health extends Component{
    render(){
        return(
            <div>
            <div  className="text15">     
            <div>
 
<h1>Food Donation</h1>
<hr/>
    <ol>
    A food bank is a non-profit, charitable organization that distributes food to 
    those who have difficulty purchasing enough to avoid hunger.
    </ol>
<ol>You can contribute for the food for the child in the Ngo who are recovering in
 the Leprosy Mission Trust India’s hospitals located at 14 places across India -Salur 
 (Andhra Pradesh), Muzaffarpur (Bihar), Chandkhuri and Champa (Chhattisgarh), Shahdara (Delhi),
  Miraj and Kothara (Maharashtra), Vadathorasalur and Dayapuram (Tamilnadu),
   Ayodhya, Barabanki and Prayagraj (Uttar Pradesh), Almora (Uttarakhand), 
   Purulia and Kolkata (West Bengal). </ol>
   <ol>Every year, 
   we receive more than 20,000 patients in our care.
    We run kitchens to feed the patients as well as the elders who are residing in the Snehalayas (Care Homes). 
    The elders were mostly deserted by their loved ones.</ol>

<ol>In the nationwide coronavirus lockdown and thereafter we have distributed
 cooked food and grocery kits to more than 8000 families affected by leprosy and disabilities.
</ol>
<ol>
The growth of food banks has been welcomed by commentators who see them as examples of an active, caring citizenship.
 Other academics and commentators have expressed concern that the rise of foodbanks may erode political support for
  welfare provision.</ol>
  <ol> Researchers have reported that in some cases food banks 
can be inefficient compared with state-run welfare,</ol>
<ol> and that some people feel ashamed at having to use them.
</ol>
</div>
<Link to="/donation-health"> <button style={{marginRight: "950px"}}  className="btn btn-info">Prev </button></Link>
 <Link to="/PaymentForm"> <button style={{marginLeft: "650px"}}   className="btn btn-success">Donate </button></Link>
 <Link to="/donation-other"> <button style={{marginRight: "350px"}}   className="btn btn-danger">Next</button></Link>
 
        </div>
        </div>
        
        );
        }
        }
        export default Health;