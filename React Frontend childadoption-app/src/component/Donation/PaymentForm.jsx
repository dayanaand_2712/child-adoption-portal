import React from 'react';
import Cards from 'react-credit-cards';
import 'react-credit-cards/es/styles-compiled.css';
import {BrowserRouter as Router , Switch,Route,Link} from "react-router-dom";
import { Label } from 'reactstrap';
import '../../App.css';
import image1 from '../../images/don1.png';
export default class PaymentForm extends React.Component {
  state = {
    cvc: '',
    expiry: '',
    focus: '',
    name: '',
    number: '',
  };
 
  handleInputFocus = (e) => {
    this.setState({ focus: e.target.name });
  }
  
  handleInputChange = (e) => {
    const { name, value } = e.target;
    
    this.setState({ [name]: value });
  }
  
  render() {
    return (
      <div>
        <center>
              <h1> Donate As per your Wish </h1>
        </center>
        <hr />
        <div style={{marginLeft: "565px"}}>
            <img  src={image1} alt="alt logo" width="20%"/>
            
          
         </div>

     
    
            <div id="PaymentForm">
                <div className = "card col-md-6 offset-md-3">
                    <div style={{marginLeft: "265px"}}>
                   
                        <Cards 
                        cvc={this.state.cvc}
                        expiry={this.state.expiry}
                        focused={this.state.focus}
                        name={this.state.name}
                        number={this.state.number}
                        />
                       
                 
                        <hr/>
                        <form>
                        <div  className="row">
                            <div  className="col">
                            <Label><b>Enter the Card Number :</b></Label>
                            </div>  
                            <div  className="col">
                                <input
                                    type="tel"
                                    name="number"
                                    placeholder="Card Number"
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                            </div>
                        </div>
                        <div  className="row">
                           <div  className="col">
                              <Label><b>Enter the Card Name :</b></Label>
                            </div>  
                            <div className="col">
                                <input
                                    type="text"
                                    name="name"
                                    placeholder="Card Name"
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                            </div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <Label><b>Enter the Expiry Date :</b></Label>
                            </div>  
                            <div className="col">
                                <input
                                    type="tel"
                                    name="expiry"
                                    placeholder="Card Expiry"
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                            
                            </div>
                        </div> 

                        <div  className="row">
                            <div  className="col">
                                <Label><b>Enter the Cvv :</b></Label>
                            </div>  
                            <div className="col" >
                                <input
                                    type="tel"
                                    name="cvc"
                                    placeholder="Card Cvv"
                                    onChange={this.handleInputChange}
                                    onFocus={this.handleInputFocus}
                                />
                            </div>
                        </div>
                        
                        
                        </form>
                       
                    </div>    
                   
                </div>    
               
                <div>
  
             <center> <Link to="/donation-other"> <button style={{marginRight: "950px"}}  className="btn btn-info">Prev </button></Link></center>  
  <Link to="/successfull"> <button style={{marginLeft: "850px"}}  className="btn btn-success">Pay now</button> </Link>&emsp;
  <Link to="/donation">  <button style={{marginRight: "750px"}}  className="btn btn-danger">Cancel </button></Link>
 
   </div>  

          </div>
         
          </div>
    
        );
        
      }
}