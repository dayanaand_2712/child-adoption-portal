import React, { Component } from 'react'
import DonationService from "../../services/donationService.js";

class RegisterUserComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
           // donarId:'',
            donarName:'',
            donationPurpose:'',
            amount:'',
            ngo:''
            
        }
        this.saveUser = this.saveUser.bind(this);
    }
    saveUser = (e) => {
        e.preventDefault();
        let donation = {//donarId: this.state.donarId,
            donarName: this.state.donarName,
            donationPurpose: this.state.donationPurpose,
            amount: this.state.amount
        };

        let user={
            donation,
            ngoId:this.state.ngo
        }
           
             console.log(user);
        DonationService.addUser(user)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/PaymentForm');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });
    render() {
        return( 
            <div className="register-user">
                <h2 className="text-center">Register Donation</h2>
                <form>
              

                <div className="form-group">
                    <label>Donar Name</label>
                    <input placeholder="Donar Name" name="donarName" className="form-control" value={this.state.donarName} onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label>Donation Purpose</label>
                    <input placeholder="Donation Purpose" name="donationPurpose" className="form-control" value={this.state.donationPurpose} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Amount</label>
                    <input type="Amount" placeholder="Amount" name="amount" className="form-control" value={this.state.amount} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Ngo Id:</label>
                    <input type="number" placeholder="NgoId" name="ngo" className="form-control" value={this.state.ngo} onChange={this.onChange}/>
                </div>

               

                <button className="btn btn-success" onClick={this.saveUser}>Card Details</button>
            </form>
    </div>
        );
    }
}

export default RegisterUserComponent;