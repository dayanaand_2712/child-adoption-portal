import React , { Component } from 'react';
import { Link } from 'react-router-dom';

//import './parenthome.css';
import '../../App.css';


class  Donation extends Component{
    render(){
        return(
            <div>
            <div  className="text12">     
            <div>
 
<h1>About Donations:</h1>
<hr/>
<ol>
    It’s unknown exactly how many babies are abandoned each year in India,
     with some sources reporting up to 11 million children, 
   but it is known that over 90% of them are girls.</ol>
     <ol>Babies are abandoned for many reasons:
       victims of rape or unmarried mothers fearing recriminations,
  being unable to financially support a child or simply not wanting </ol>
  <ol>another girl in the family.</ol>
<h3>
How we help</h3>

 
<ol>
Many babies are left at our centre, and our ChildLine team and volunteers also bring us babies
 found abandoned in public places, 
sadly sometimes they are found too late. </ol>
<ol>Many of the infants who survive are very fragile and/or premature and require hospitalisation and intensive care.</ol> 
<ol>Fortunately, most survive and we start the process of searching for their family and if found counsel them on the 
next steps.</ol>
<ol>
Our adoption process finds loving families for those unwanted and we’re with them every step
 of the way ensuring a safe and smooth transition to their new home.
</ol>
 <h3>
 Who we help   
 </h3>
<ol>
Since we opened our doors in 2004 we have successfully rescued and saved the lives of over 500 babies,
 adopting them under
a government approved and accredited process.</ol>
  <ol>We have also played a key role consulting on the government’s proposed new adoption guidelines.</ol>

<h3>How you can help
</h3>
<ol>
Help us to ensure no baby is left abandoned and unloved.</ol>
<ol>
 We are currently building a new 50-bed adoption centre with improved neo- and 
 post-natal 
 facilities </ol>
 <ol>and space for unmarried 
 mothers to give birth in safety.</ol>
 <Link to="/donation"> <button style={{marginRight: "950px"}}  className="btn btn-info">Prev </button></Link>
 <Link to="/PaymentForm"> <button style={{marginLeft: "650px"}}   className="btn btn-success">Donate </button></Link>
 <Link to="/donation-education"> <button style={{marginRight: "350px"}}   className="btn btn-danger">Next</button></Link>
        </div>
        </div>
        </div>
       
        );
        }
        }
        export default Donation;