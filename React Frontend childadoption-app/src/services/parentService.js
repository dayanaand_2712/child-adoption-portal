import axios from 'axios';

//const USER_API_BASE_URL = 'http://localhost:7070/ngo/login';
const USER_API_BASE_URL = 'http://localhost:4005/parent';



class ParentService {
    addUser(user){
      console.log(user)
        return axios.post(USER_API_BASE_URL+"/register",user);
    }

    login(email, password) {
        return axios
          .post(USER_API_BASE_URL + "/login", {
            email,
            password
          })
          .then(response => {
            if (response.data.accessToken) {
              localStorage.setItem("parent", JSON.stringify(response.data));
            }
    
            return response.data;
          });
      }
}
export default new ParentService();