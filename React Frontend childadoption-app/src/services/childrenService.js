import axios from 'axios';


const CHILD_API_BASE_URL = 'http://localhost:4005/child';

class ChildService {
    addChild(child){
        return axios.post(CHILD_API_BASE_URL+"/register",child);
    }

    getChildList(){
        return axios.get(CHILD_API_BASE_URL+"/list");
    }

    getChildById(child_id){
        return axios.get(CHILD_API_BASE_URL+"/display/"+child_id);
    }

    updateChild(child){
        return axios.put(CHILD_API_BASE_URL+"/update/"+child.child_id,child);
    }

    deleteChild(child_id){
        return axios.delete(CHILD_API_BASE_URL+"/delete/"+child_id);
    }

    getChildByNgoId(ngo_id){
        return axios.get(CHILD_API_BASE_URL+"/childbyngoid/"+ngo_id);
    }
}
export default new ChildService();