import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:4005/ngo';



class NgoService {
    addUser(user){
      return axios.post(USER_API_BASE_URL+"/register",user);
    }

    login(email, password) {
        return axios
          .post(USER_API_BASE_URL + "/login", {
            email,
            password
          })
          .then(response => {
            if (response.data.accessToken) {
              localStorage.setItem("ngo", JSON.stringify(response.data));
            }
    
            return response.data;
          });
      }

      getChild(){
        return axios.get(USER_API_BASE_URL+"/childlist");
      }

      getNgo(){
      return axios.get(USER_API_BASE_URL+"/ngolist");
      }

      getNgoByEmail(email){
        return axios.get(USER_API_BASE_URL+"/getByEmail/"+email);
      }

      deleteNgo(ngo_id){
        return axios.delete(USER_API_BASE_URL+"/delete/"+ngo_id);
      }
}
export default new NgoService();