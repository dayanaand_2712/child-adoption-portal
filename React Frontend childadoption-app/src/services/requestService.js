import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:4005/request';


class RequestService {
    addUser(user){
        return axios.post(USER_API_BASE_URL+"/certainchildtongo",user);
    }

    getReqList(){
        return axios.get(USER_API_BASE_URL+"/list");
    }

    getReqById(req_id){
        return axios.get(USER_API_BASE_URL+"/viewlist/"+req_id);
    }

    getReqByNgoId(ngo_id){
        return axios.get(USER_API_BASE_URL+"/reqbyngoid/"+ngo_id);
    }

    deleteReqById(req_id){
        return axios.delete(USER_API_BASE_URL+"/delete/"+req_id);
    }

    resFromNgo(request){
        return axios.post(USER_API_BASE_URL+"/responsefromngo",request);
    }

    update(req_id,request){
        return axios.put(USER_API_BASE_URL+"/update/"+req_id,request);
    }



}
export default new RequestService();